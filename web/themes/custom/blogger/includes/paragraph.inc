<?php

/**
 * @file
 * Preprocesses for paragraphs.
 */

/**
 * Implements hook_preprocess__HOOK() for paragraph-code-html.twig.
 */
function blogger_preprocess_paragraph__code(&$variables) {
 $variables['#attached']['library'][] = 'blogger/paragraph.code';
}
