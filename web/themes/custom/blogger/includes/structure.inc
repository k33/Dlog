<?php

/**
 * @file
 * Theme and preprocess functions for breadcrumbs, messages, tabs..etc
 */

use \Drupal\Core\Template\Attribute;
use Drupal\node\NodeInterface;

/**
 * Implements hook_preprocess_HOOK() for page html twig.
 */
function blogger_preprocess_page(array &$variables) {
  $main_layout_attributes = new Attribute();

  // Set default class.
  $main_layout_attributes->addClass('main-layout');

  // Handle sidebar modifiers.
  if (!empty($variables['page']['sidebar_first'])) {
    $main_layout_attributes->addClass('main-layout--sidebar-first');
  }
  else {
    $main_layout_attributes->addClass('main-layout--no-sidebars');
  }

  $variables['main_layout_attributes'] = $main_layout_attributes;
}

/**
 * Implements hook_preprocess_HOOK() for front-page.html.twig.
 */
function blogger_preprocess_page__front(array &$variables) {
  /** @var \Drupal\media\MediaStorage $media_storage */
  $media_storage = \Drupal::entityTypeManager()->getStorage('media');
  /** @var \Drupal\media\MediaInterface $media_image */
  $media_image = $media_storage->load(25);
  /** @var \Drupal\media\MediaInterface $media_image */
  $media_video = $media_storage->load(26);

  /** @var \Drupal\media\MediaInterface $media_image */
  $media_image_author = $media_storage->load(27);

  /** @var \Drupal\media\MediaInterface $media_image */
  $media_image_author_background = $media_storage->load(28);

  $variables['search_image_uri'] = $media_image->get('field_media_image')
    ->entity->getFileUri();

  $variables['search_video_uri'] = $media_video->get('field_media_video_file')
    ->entity->getFileUri();

  $variables['author_avatar'] = $media_image_author->get('field_media_image')
    ->entity->getFileUri();

  $variables['author_bg'] = $media_image_author_background->get('field_media_image')
    ->entity->getFileUri();
}

/**
 * Implements hook_preprocess_HOOK() for page--contact.html.twig.
 */
function blogger_preprocess_page__contact(array &$variables) {
  /** @var \Drupal\media\MediaStorage $media_storage */
  $media_storage = \Drupal::entityTypeManager()->getStorage('media');
  /** @var \Drupal\media\MediaInterface $media_image */
  $media_image = $media_storage->load(29);
  /** @var \Drupal\media\MediaInterface $media_image */
  $media_video = $media_storage->load(30);

  $variables['contact_image_uri'] = $media_image->get('field_media_image')
    ->entity->getFileUri();

  $variables['contact_video_uri'] = $media_video->get('field_media_video_file')
    ->entity->getFileUri();

}

/**
 * @file
 * Theme and preprocess functions for nodes
 */

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function blogger_theme_suggestions_page_alter(array &$suggestions, array $variables) {

  $node = \Drupal::routeMatch()->getParameter('node');
  if ($node instanceof NodeInterface) {
    if ($node->id() == 1) {
      $suggestions[] = 'page__about';
    }
  }

}

/**
 * Implements hook_preprocess_HOOK() for page--contact.html.twig.
 */
function blogger_preprocess_page__about(array &$variables) {
  /** @var \Drupal\media\MediaStorage $media_storage */
  $media_storage = \Drupal::entityTypeManager()->getStorage('media');
  /** @var \Drupal\media\MediaInterface $media_image */
  $media_image = $media_storage->load(32);

  $media_image_signature = $media_storage->load(33);

  $variables['about_image_uri'] = $media_image->get('field_media_image')
    ->entity->getFileUri();

  $variables['about_signature_image_uri'] = $media_image_signature->get('field_media_image')
    ->entity->getFileUri();

}
