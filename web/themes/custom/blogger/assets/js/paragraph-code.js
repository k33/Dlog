/**
 * @file
 * Paragraph  code highlight behaviors.
 */
(function ($, Drupal,highlight) {
  Drupal.behaviors.paragraphHiggloght = {
    attach : function (context, settings) {
      const elements = $(context).find('pre code').once('highlight');

      if (elements) {
        $.each(elements, ($key, element) => {
        hljs.highlightBlock(element);
        });
      }
    }
  };
}(jQuery, Drupal));
