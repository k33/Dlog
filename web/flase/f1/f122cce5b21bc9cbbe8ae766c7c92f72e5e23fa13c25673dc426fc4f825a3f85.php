<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/blogger/templates/theme/dlog-previous-next--blog-article.html.twig */
class __TwigTemplate_0d03e5b613d1c7bcee0b2363694d228e07ae073e220ab7c07d740192354dd532 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1, "if" => 4, "for" => 11];
        $filters = ["escape" => 2, "image_style" => 6];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for'],
                ['escape', 'image_style'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["bem_block"] = "previous-next";
        // line 2
        echo "<div ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["bem_block"] ?? null), 1 => ($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "--blog-article")], "method")), "html", null, true);
        echo ">
  <div class=\"";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__previous\">
    ";
        // line 4
        if (($context["previous"] ?? null)) {
            // line 5
            echo "      <a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["previous"] ?? null), "url", [])), "html", null, true);
            echo "\" class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__link ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__link--previous\">
        <div class=\"";
            // line 6
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__promo-image\" style=\"background-image: url(";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\twig_tweak\TwigExtension')->imageStyle($this->sandbox->ensureToStringAllowed($this->getAttribute(($context["previous"] ?? null), "promo_uri", [])), "previous_next_blog_article_image"), "html", null, true);
            echo ")\"></div>
        <div class=\"";
            // line 7
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__icon ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__icon--previous\"></div>
        <div class=\"";
            // line 8
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__content ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__content--previous\">
          ";
            // line 9
            if ($this->getAttribute(($context["previous"] ?? null), "tags", [])) {
                // line 10
                echo "            <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
                echo "__tags\">
              ";
                // line 11
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["previous"] ?? null), "tags", []));
                foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                    // line 12
                    echo "                <span class=\"";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
                    echo "_tag\">";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["tag"], "label", [])), "html", null, true);
                    echo "</span>
              ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 14
                echo "            </div>
          ";
            }
            // line 16
            echo "          ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["previous"] ?? null), "label", [])), "html", null, true);
            echo "
        </div>
      </a>
    ";
        }
        // line 20
        echo "  </div>
  <div class=\"";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__next\">
    ";
        // line 22
        if (($context["next"] ?? null)) {
            // line 23
            echo "      <a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["next"] ?? null), "url", [])), "html", null, true);
            echo "\" class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__link ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__link--next\">
        <div class=\"";
            // line 24
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__promo-image\" style=\"background-image: url(";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\twig_tweak\TwigExtension')->imageStyle($this->sandbox->ensureToStringAllowed($this->getAttribute(($context["next"] ?? null), "promo_uri", [])), "previous_next_blog_article_image"), "html", null, true);
            echo ")\"></div>
        <div class=\"";
            // line 25
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__content ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__content--next\">
          ";
            // line 26
            if ($this->getAttribute(($context["next"] ?? null), "tags", [])) {
                // line 27
                echo "            <div class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
                echo "__tags\">
              ";
                // line 28
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute(($context["next"] ?? null), "tags", []));
                foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                    // line 29
                    echo "                <span class=\"";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
                    echo "_tag\">";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["tag"], "label", [])), "html", null, true);
                    echo "</span>
              ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 31
                echo "            </div>
          ";
            }
            // line 33
            echo "          ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["next"] ?? null), "label", [])), "html", null, true);
            echo "
        </div>
        <div class=\"";
            // line 35
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__icon ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__icon--next\"></div>
      </a>
    ";
        }
        // line 38
        echo "  </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "themes/custom/blogger/templates/theme/dlog-previous-next--blog-article.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  199 => 38,  191 => 35,  185 => 33,  181 => 31,  170 => 29,  166 => 28,  161 => 27,  159 => 26,  153 => 25,  147 => 24,  138 => 23,  136 => 22,  132 => 21,  129 => 20,  121 => 16,  117 => 14,  106 => 12,  102 => 11,  97 => 10,  95 => 9,  89 => 8,  83 => 7,  77 => 6,  68 => 5,  66 => 4,  62 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% set bem_block  = 'previous-next' %}
<div {{ attributes.addClass(bem_block, bem_block ~ '--blog-article') }}>
  <div class=\"{{ bem_block  }}__previous\">
    {% if previous %}
      <a href=\"{{ previous.url }}\" class=\"{{ bem_block }}__link {{ bem_block }}__link--previous\">
        <div class=\"{{ bem_block }}__promo-image\" style=\"background-image: url({{ previous.promo_uri|image_style('previous_next_blog_article_image') }})\"></div>
        <div class=\"{{ bem_block }}__icon {{ bem_block }}__icon--previous\"></div>
        <div class=\"{{ bem_block }}__content {{ bem_block }}__content--previous\">
          {% if previous.tags %}
            <div class=\"{{ bem_block }}__tags\">
              {% for tag in previous.tags %}
                <span class=\"{{ bem_block }}_tag\">{{ tag.label }}</span>
              {% endfor %}
            </div>
          {% endif %}
          {{ previous.label }}
        </div>
      </a>
    {% endif %}
  </div>
  <div class=\"{{ bem_block  }}__next\">
    {% if next %}
      <a href=\"{{ next.url }}\" class=\"{{ bem_block }}__link {{ bem_block }}__link--next\">
        <div class=\"{{ bem_block }}__promo-image\" style=\"background-image: url({{ next.promo_uri|image_style('previous_next_blog_article_image') }})\"></div>
        <div class=\"{{ bem_block }}__content {{ bem_block }}__content--next\">
          {% if next.tags %}
            <div class=\"{{ bem_block }}__tags\">
              {% for tag in next.tags %}
                <span class=\"{{ bem_block }}_tag\">{{ tag.label }}</span>
              {% endfor %}
            </div>
          {% endif %}
          {{ next.label }}
        </div>
        <div class=\"{{ bem_block }}__icon {{ bem_block }}__icon--next\"></div>
      </a>
    {% endif %}
  </div>
</div>

", "themes/custom/blogger/templates/theme/dlog-previous-next--blog-article.html.twig", "/var/www/web/themes/custom/blogger/templates/theme/dlog-previous-next--blog-article.html.twig");
    }
}
