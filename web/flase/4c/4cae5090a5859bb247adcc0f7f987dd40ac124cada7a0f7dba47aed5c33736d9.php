<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @blogger/include/pages/about.html.twig */
class __TwigTemplate_2a65d03b00b5fbf120cfbc58635a6addcf27c59752aeb90bbce2e46248c1da0c extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1];
        $filters = ["escape" => 2, "image_style" => 13];
        $functions = ["url" => 90];

        try {
            $this->sandbox->checkSecurity(
                ['set'],
                ['escape', 'image_style'],
                ['url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["bem_block"] = "about-page";
        // line 2
        echo "<div class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "\">
  <div class=\"";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__inner\">
    <div class=\"";
        // line 4
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__left\">
      <h1 class=\"";
        // line 5
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__title\">Об аторе</h1>
      <div class=\"";
        // line 6
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__quote\">
        <div class=\"";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__quote-cite\">
          “The Box III cements Hooli’s legacy of innovation. And my signature — that’s a promise from me to the Hooli user.
          To buck the impossible. To keep imagining new ways to change the world. To keep striving for a better tomorrow.
          To stay restless, never settling for the status quo in data storage devices.”
        </div>
        <div class=\"";
        // line 12
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__quote-author\">— Gavin Belson, Founder and CEO</div>
        <img src=\"";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\twig_tweak\TwigExtension')->imageStyle($this->sandbox->ensureToStringAllowed(($context["about_signature_image_uri"] ?? null)), "thumbnail"), "html", null, true);
        echo "\" class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__quote-signature\" alt=\"Gavin Balson Signature\">
      </div>
      <div class=\"";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills\">
        <div class=\"";
        // line 16
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-title\">Мои <span>навики</span></div>
        <div class=\"";
        // line 17
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-items\">
          <div class=\"";
        // line 18
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item\">
            <div class=\"";
        // line 19
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-icon  ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-icon--drupal\"></div>
            <div class=\"";
        // line 20
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-title\">Drupal</div>
            <div class=\"";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-description\">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis maxime, nemo! Animi eum facere,
              fugiat in inventore iste magni molestiae natus neque odit omnis, quas ratione, tempore tenetur veniam
              vitae.
            </div>
          </div>
          <div class=\"";
        // line 27
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item\">
            <div class=\"";
        // line 28
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-icon  ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-icon--php\"></div>
            <div class=\"";
        // line 29
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-title\">PHP</div>
            <div class=\"";
        // line 30
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-description\">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis maxime, nemo! Animi eum facere,
              fugiat in inventore iste magni molestiae natus neque odit omnis, quas ratione, tempore tenetur veniam
              vitae.
            </div>
          </div>
          <div class=\"";
        // line 36
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item\">
            <div class=\"";
        // line 37
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-icon  ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-icon--html\"></div>
            <div class=\"";
        // line 38
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-title\">HTML</div>
            <div class=\"";
        // line 39
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-description\">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis maxime, nemo! Animi eum facere,
              fugiat in inventore iste magni molestiae natus neque odit omnis, quas ratione, tempore tenetur veniam
              vitae.
            </div>
          </div>
          <div class=\"";
        // line 45
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item\">
            <div class=\"";
        // line 46
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-icon  ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-icon--css\"></div>
            <div class=\"";
        // line 47
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-title\">CSS/SCSS</div>
            <div class=\"";
        // line 48
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-description\">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis maxime, nemo! Animi eum facere,
              fugiat in inventore iste magni molestiae natus neque odit omnis, quas ratione, tempore tenetur veniam
              vitae.
            </div>
          </div>
          <div class=\"";
        // line 54
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item\">
            <div class=\"";
        // line 55
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-icon  ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-icon--js\"></div>
            <div class=\"";
        // line 56
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-title\">JavaScript</div>
            <div class=\"";
        // line 57
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-description\">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis maxime, nemo! Animi eum facere,
              fugiat in inventore iste magni molestiae natus neque odit omnis, quas ratione, tempore tenetur veniam
              vitae.
            </div>
          </div>
          <div class=\"";
        // line 63
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item\">
            <div class=\"";
        // line 64
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-icon  ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-icon--server\"></div>
            <div class=\"";
        // line 65
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-title\">Администрирование серверов</div>
            <div class=\"";
        // line 66
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__skills-item-description\">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis maxime, nemo! Animi eum facere,
              fugiat in inventore iste magni molestiae natus neque odit omnis, quas ratione, tempore tenetur veniam
              vitae.
            </div>
          </div>
        </div>
      </div>
      <div class=\"";
        // line 74
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__services\">
        <h2 class=\"";
        // line 75
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__services-title\">Мои <span>услуги</span></h2>
        <div class=\"";
        // line 76
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__prices\">
          <div class=\"";
        // line 77
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price\">
            <div class=\"";
        // line 78
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-icon ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-icon--hour-rate\"></div>
              <div class=\"";
        // line 79
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-title\">Разработка</div>
              <span class=\"";
        // line 80
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-price-amount\">1 500</span>
              <span class=\"";
        // line 81
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-price-currency\">руб</span>
              <span class=\"";
        // line 82
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-price-for\">час</span>
            <ul class=\"";
        // line 83
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-services\">
              <li class=\"";
        // line 84
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-service\">Написания кастомних молуей</li>
              <li class=\"";
        // line 85
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-service\">Верстка</li>
              <li class=\"";
        // line 86
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-service\">Настройка и деплой</li>
              <li class=\"";
        // line 87
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-service\">Рефакторинг и оптимизация</li>
              <li class=\"";
        // line 88
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-service\">Доработка чущих модулей</li>
            </ul>
            <a href=\"";
        // line 90
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("contact.site_page"));
        echo "\" class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__prices-contact\">Написать</a>
          </div>
          <div class=\"";
        // line 92
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price\">
            <div class=\"";
        // line 93
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-icon ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-icon--audit\"></div>
              <div class=\"";
        // line 94
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-title\">Анализ кода и аудит</div>
              <span class=\"";
        // line 95
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-price-amount\">5000</span>
              <span class=\"";
        // line 96
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-price-currency\">руб</span>
              <span class=\"";
        // line 97
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-price-for\">час</span>
            <ul class=\"";
        // line 98
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-services\">
              <li class=\"";
        // line 99
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-service\">Анализ и аудит кода</li>
              <li class=\"";
        // line 100
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-service\">Документ сочетом</li>
              <li class=\"";
        // line 101
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-service\">90 минут на розгвор по скайпу</li>
              <li class=\"";
        // line 102
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-service\">Различние очети от инструментов с проверками</li>
              <li class=\"";
        // line 103
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__price-service\">Примери модулей</li>
            </ul>
            <a href=\"";
        // line 105
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("contact.site_page"));
        echo "\" class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__prices-contact\">Написать</a>
          </div>
        </div>
      </div>
    </div>
    <div class=\"";
        // line 110
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__right\">
      <img src=\"";
        // line 111
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\twig_tweak\TwigExtension')->imageStyle($this->sandbox->ensureToStringAllowed(($context["about_image_uri"] ?? null)), "dlog_hero"), "html", null, true);
        echo "\" alt=\"about\" title=\"about\" class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__about-poster\">
    </div>
  </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "@blogger/include/pages/about.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  384 => 111,  380 => 110,  370 => 105,  365 => 103,  361 => 102,  357 => 101,  353 => 100,  349 => 99,  345 => 98,  341 => 97,  337 => 96,  333 => 95,  329 => 94,  323 => 93,  319 => 92,  312 => 90,  307 => 88,  303 => 87,  299 => 86,  295 => 85,  291 => 84,  287 => 83,  283 => 82,  279 => 81,  275 => 80,  271 => 79,  265 => 78,  261 => 77,  257 => 76,  253 => 75,  249 => 74,  238 => 66,  234 => 65,  228 => 64,  224 => 63,  215 => 57,  211 => 56,  205 => 55,  201 => 54,  192 => 48,  188 => 47,  182 => 46,  178 => 45,  169 => 39,  165 => 38,  159 => 37,  155 => 36,  146 => 30,  142 => 29,  136 => 28,  132 => 27,  123 => 21,  119 => 20,  113 => 19,  109 => 18,  105 => 17,  101 => 16,  97 => 15,  90 => 13,  86 => 12,  78 => 7,  74 => 6,  70 => 5,  66 => 4,  62 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% set bem_block = 'about-page' %}
<div class=\"{{ bem_block }}\">
  <div class=\"{{ bem_block }}__inner\">
    <div class=\"{{ bem_block }}__left\">
      <h1 class=\"{{ bem_block }}__title\">Об аторе</h1>
      <div class=\"{{ bem_block }}__quote\">
        <div class=\"{{ bem_block }}__quote-cite\">
          “The Box III cements Hooli’s legacy of innovation. And my signature — that’s a promise from me to the Hooli user.
          To buck the impossible. To keep imagining new ways to change the world. To keep striving for a better tomorrow.
          To stay restless, never settling for the status quo in data storage devices.”
        </div>
        <div class=\"{{ bem_block }}__quote-author\">— Gavin Belson, Founder and CEO</div>
        <img src=\"{{about_signature_image_uri|image_style ('thumbnail')}}\" class=\"{{ bem_block }}__quote-signature\" alt=\"Gavin Balson Signature\">
      </div>
      <div class=\"{{ bem_block }}__skills\">
        <div class=\"{{ bem_block }}__skills-title\">Мои <span>навики</span></div>
        <div class=\"{{ bem_block }}__skills-items\">
          <div class=\"{{ bem_block }}__skills-item\">
            <div class=\"{{ bem_block }}__skills-item-icon  {{ bem_block }}__skills-item-icon--drupal\"></div>
            <div class=\"{{ bem_block }}__skills-item-title\">Drupal</div>
            <div class=\"{{ bem_block }}__skills-item-description\">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis maxime, nemo! Animi eum facere,
              fugiat in inventore iste magni molestiae natus neque odit omnis, quas ratione, tempore tenetur veniam
              vitae.
            </div>
          </div>
          <div class=\"{{ bem_block }}__skills-item\">
            <div class=\"{{ bem_block }}__skills-item-icon  {{ bem_block }}__skills-item-icon--php\"></div>
            <div class=\"{{ bem_block }}__skills-item-title\">PHP</div>
            <div class=\"{{ bem_block }}__skills-item-description\">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis maxime, nemo! Animi eum facere,
              fugiat in inventore iste magni molestiae natus neque odit omnis, quas ratione, tempore tenetur veniam
              vitae.
            </div>
          </div>
          <div class=\"{{ bem_block }}__skills-item\">
            <div class=\"{{ bem_block }}__skills-item-icon  {{ bem_block }}__skills-item-icon--html\"></div>
            <div class=\"{{ bem_block }}__skills-item-title\">HTML</div>
            <div class=\"{{ bem_block }}__skills-item-description\">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis maxime, nemo! Animi eum facere,
              fugiat in inventore iste magni molestiae natus neque odit omnis, quas ratione, tempore tenetur veniam
              vitae.
            </div>
          </div>
          <div class=\"{{ bem_block }}__skills-item\">
            <div class=\"{{ bem_block }}__skills-item-icon  {{ bem_block }}__skills-item-icon--css\"></div>
            <div class=\"{{ bem_block }}__skills-item-title\">CSS/SCSS</div>
            <div class=\"{{ bem_block }}__skills-item-description\">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis maxime, nemo! Animi eum facere,
              fugiat in inventore iste magni molestiae natus neque odit omnis, quas ratione, tempore tenetur veniam
              vitae.
            </div>
          </div>
          <div class=\"{{ bem_block }}__skills-item\">
            <div class=\"{{ bem_block }}__skills-item-icon  {{ bem_block }}__skills-item-icon--js\"></div>
            <div class=\"{{ bem_block }}__skills-item-title\">JavaScript</div>
            <div class=\"{{ bem_block }}__skills-item-description\">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis maxime, nemo! Animi eum facere,
              fugiat in inventore iste magni molestiae natus neque odit omnis, quas ratione, tempore tenetur veniam
              vitae.
            </div>
          </div>
          <div class=\"{{ bem_block }}__skills-item\">
            <div class=\"{{ bem_block }}__skills-item-icon  {{ bem_block }}__skills-item-icon--server\"></div>
            <div class=\"{{ bem_block }}__skills-item-title\">Администрирование серверов</div>
            <div class=\"{{ bem_block }}__skills-item-description\">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis maxime, nemo! Animi eum facere,
              fugiat in inventore iste magni molestiae natus neque odit omnis, quas ratione, tempore tenetur veniam
              vitae.
            </div>
          </div>
        </div>
      </div>
      <div class=\"{{ bem_block }}__services\">
        <h2 class=\"{{ bem_block }}__services-title\">Мои <span>услуги</span></h2>
        <div class=\"{{ bem_block }}__prices\">
          <div class=\"{{ bem_block }}__price\">
            <div class=\"{{ bem_block }}__price-icon {{ bem_block }}__price-icon--hour-rate\"></div>
              <div class=\"{{ bem_block }}__price-title\">Разработка</div>
              <span class=\"{{ bem_block }}__price-price-amount\">1 500</span>
              <span class=\"{{ bem_block }}__price-price-currency\">руб</span>
              <span class=\"{{ bem_block }}__price-price-for\">час</span>
            <ul class=\"{{ bem_block }}__price-services\">
              <li class=\"{{ bem_block }}__price-service\">Написания кастомних молуей</li>
              <li class=\"{{ bem_block }}__price-service\">Верстка</li>
              <li class=\"{{ bem_block }}__price-service\">Настройка и деплой</li>
              <li class=\"{{ bem_block }}__price-service\">Рефакторинг и оптимизация</li>
              <li class=\"{{ bem_block }}__price-service\">Доработка чущих модулей</li>
            </ul>
            <a href=\"{{ url('contact.site_page') }}\" class=\"{{ bem_block }}__prices-contact\">Написать</a>
          </div>
          <div class=\"{{ bem_block }}__price\">
            <div class=\"{{ bem_block }}__price-icon {{ bem_block }}__price-icon--audit\"></div>
              <div class=\"{{ bem_block }}__price-title\">Анализ кода и аудит</div>
              <span class=\"{{ bem_block }}__price-price-amount\">5000</span>
              <span class=\"{{ bem_block }}__price-price-currency\">руб</span>
              <span class=\"{{ bem_block }}__price-price-for\">час</span>
            <ul class=\"{{ bem_block }}__price-services\">
              <li class=\"{{ bem_block }}__price-service\">Анализ и аудит кода</li>
              <li class=\"{{ bem_block }}__price-service\">Документ сочетом</li>
              <li class=\"{{ bem_block }}__price-service\">90 минут на розгвор по скайпу</li>
              <li class=\"{{ bem_block }}__price-service\">Различние очети от инструментов с проверками</li>
              <li class=\"{{ bem_block }}__price-service\">Примери модулей</li>
            </ul>
            <a href=\"{{ url('contact.site_page') }}\" class=\"{{ bem_block }}__prices-contact\">Написать</a>
          </div>
        </div>
      </div>
    </div>
    <div class=\"{{ bem_block }}__right\">
      <img src=\"{{ about_image_uri|image_style('dlog_hero') }}\" alt=\"about\" title=\"about\" class=\"{{ bem_block }}__about-poster\">
    </div>
  </div>
</div>

", "@blogger/include/pages/about.html.twig", "/var/www/web/themes/custom/blogger/templates/include/pages/about.html.twig");
    }
}
