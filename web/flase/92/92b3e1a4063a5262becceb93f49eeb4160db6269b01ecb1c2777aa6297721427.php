<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/blogger/templates/include/pages/page--about.html.twig */
class __TwigTemplate_84bce99096711794e4570f2f3f679f730083e990fd4c409845cdb1005b427b21 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1, "include" => 3];
        $filters = ["escape" => 2];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'include'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["bem_block"] = "app";
        // line 2
        echo "<div class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "\">
  ";
        // line 3
        $this->loadTemplate("@blogger/include/header.html.twig", "themes/custom/blogger/templates/include/pages/page--about.html.twig", 3)->display(twig_array_merge($context, ["show_dlog_hero" => false, "bem_modifires" => [0 => "light-menu"]]));
        // line 4
        echo "  ";
        $this->loadTemplate("@blogger/include/pages/about.html.twig", "themes/custom/blogger/templates/include/pages/page--about.html.twig", 4)->display($context);
        // line 5
        echo "  ";
        $this->loadTemplate("@blogger/include/footer.html.twig", "themes/custom/blogger/templates/include/pages/page--about.html.twig", 5)->display($context);
        // line 6
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/blogger/templates/include/pages/page--about.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 6,  67 => 5,  64 => 4,  62 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% set bem_block = 'app' %}
<div class=\"{{ bem_block }}\">
  {% include '@blogger/include/header.html.twig' with {'show_dlog_hero' : false, 'bem_modifires' : ['light-menu']} %}
  {% include '@blogger/include/pages/about.html.twig' %}
  {% include '@blogger/include/footer.html.twig' %}
</div>
", "themes/custom/blogger/templates/include/pages/page--about.html.twig", "/var/www/web/themes/custom/blogger/templates/include/pages/page--about.html.twig");
    }
}
