<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/form/details.html.twig */
class __TwigTemplate_a73023f5442cb167f3748877e33a02b2d970638e3e664838108ca506bcca9f72 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1, "if" => 12];
        $filters = ["clean_class" => 1, "escape" => 5];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['clean_class', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["bem_block"] = ((($context["form_id"] ?? null)) ? (((("form-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["form_id"] ?? null)))) . "__") . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(($context["name"] ?? null))))) : ("details"));
        // line 2
        $context["classes"] = [0 =>         // line 3
($context["bem_block"] ?? null)];
        // line 5
        echo "<details";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  ";
        // line 6
        $context["bem_element_summary"] = ($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "-summary");
        // line 7
        echo "  ";
        $context["summary_classes"] = [0 =>         // line 8
($context["bem_element_summary"] ?? null), 1 => ((        // line 9
($context["required"] ?? null)) ? ("js-form-required") : ("")), 2 => ((        // line 10
($context["required"] ?? null)) ? (($this->sandbox->ensureToStringAllowed(($context["bem_element_summary"] ?? null)) . "--required")) : (""))];
        // line 12
        if (($context["title"] ?? null)) {
            // line 13
            echo "<summary";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["summary_attributes"] ?? null), "addClass", [0 => ($context["summary_classes"] ?? null)], "method")), "html", null, true);
            echo ">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null)), "html", null, true);
            echo "</summary>";
        }
        // line 16
        if (($context["errors"] ?? null)) {
            // line 17
            echo "    <div class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "-errors\">
      ";
            // line 18
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["errors"] ?? null)), "html", null, true);
            echo "
    </div>
  ";
        }
        // line 21
        echo "
  <div class=\"";
        // line 22
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "-content\">
    ";
        // line 23
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["description"] ?? null)), "html", null, true);
        echo "
    ";
        // line 24
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["children"] ?? null)), "html", null, true);
        echo "
    ";
        // line 25
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["value"] ?? null)), "html", null, true);
        echo "
  </div>
</details>
";
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/form/details.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 25,  106 => 24,  102 => 23,  98 => 22,  95 => 21,  89 => 18,  84 => 17,  82 => 16,  75 => 13,  73 => 12,  71 => 10,  70 => 9,  69 => 8,  67 => 7,  65 => 6,  60 => 5,  58 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% set bem_block = form_id ? 'form-' ~ form_id|clean_class ~ '__' ~ name|clean_class : 'details' %}
{% set classes = [
  bem_block,
] %}
<details{{ attributes.addClass(classes) }}>
  {% set bem_element_summary = bem_block ~ '-summary' %}
  {% set summary_classes = [
    bem_element_summary,
    required ? 'js-form-required',
    required ? bem_element_summary ~ '--required',
  ] %}
  {%- if title -%}
    <summary{{ summary_attributes.addClass(summary_classes) }}>{{ title }}</summary>
  {%- endif -%}

  {% if errors %}
    <div class=\"{{ bem_block }}-errors\">
      {{ errors }}
    </div>
  {% endif %}

  <div class=\"{{ bem_block }}-content\">
    {{ description }}
    {{ children }}
    {{ value }}
  </div>
</details>
", "themes/contrib/glisseo/templates/form/details.html.twig", "/var/www/web/themes/contrib/glisseo/templates/form/details.html.twig");
    }
}
