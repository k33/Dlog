<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/form/datetime-wrapper.html.twig */
class __TwigTemplate_037af12bbfc2da772239c513c1dfc80dbac2e20eb9313ee6e98d2392681aebe4 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1, "if" => 6];
        $filters = ["escape" => 7];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["title_classes"] = [0 => "form-item__label", 1 => ((        // line 3
($context["required"] ?? null)) ? ("js-form-required") : ("")), 2 => ((        // line 4
($context["required"] ?? null)) ? ("form-required") : (""))];
        // line 6
        if (($context["title"] ?? null)) {
            // line 7
            echo "  <label";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title_attributes"] ?? null), "addClass", [0 => ($context["title_classes"] ?? null)], "method")), "html", null, true);
            echo ">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null)), "html", null, true);
            echo "</label>
";
        }
        // line 9
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content"] ?? null)), "html", null, true);
        echo "
";
        // line 10
        if (($context["errors"] ?? null)) {
            // line 11
            echo "  <div class=\"form-item__errors\">
    ";
            // line 12
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["errors"] ?? null)), "html", null, true);
            echo "
  </div>
";
        }
        // line 15
        if (($context["description"] ?? null)) {
            // line 16
            echo "  <div class=\"form-item__description\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["description"] ?? null)), "html", null, true);
            echo "</div>
";
        }
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/form/datetime-wrapper.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 16,  84 => 15,  78 => 12,  75 => 11,  73 => 10,  69 => 9,  61 => 7,  59 => 6,  57 => 4,  56 => 3,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% set title_classes = [
  'form-item__label',
  required ? 'js-form-required',
  required ? 'form-required',
] %}
{% if title %}
  <label{{ title_attributes.addClass(title_classes) }}>{{ title }}</label>
{% endif %}
{{ content }}
{% if errors %}
  <div class=\"form-item__errors\">
    {{ errors }}
  </div>
{% endif %}
{% if description %}
  <div class=\"form-item__description\">{{ description }}</div>
{% endif %}
", "themes/contrib/glisseo/templates/form/datetime-wrapper.html.twig", "/var/www/web/themes/contrib/glisseo/templates/form/datetime-wrapper.html.twig");
    }
}
