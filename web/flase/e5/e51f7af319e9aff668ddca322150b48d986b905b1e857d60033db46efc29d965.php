<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/blogger/templates/content/node/blog-article/field--node--field-comments--blog-article.html.twig */
class __TwigTemplate_a5cac8c4f17957460c9d710b1f736b2974cdb4a9ba0dba593ee7df5a942316c4 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "if" => 9];
        $filters = ["clean_class" => 2, "replace" => 2, "escape" => 8, "t" => 11];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['clean_class', 'replace', 'escape', 't'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["bem_block"] = ((\Drupal\Component\Utility\Html::getClass(((($this->sandbox->ensureToStringAllowed(($context["entity_type"] ?? null)) . "-") . $this->sandbox->ensureToStringAllowed(($context["bundle"] ?? null))) . (((($context["view_mode"] ?? null) != "default")) ? (("-" . $this->sandbox->ensureToStringAllowed(($context["view_mode"] ?? null)))) : ("")))) . "__") . \Drupal\Component\Utility\Html::getClass(twig_replace_filter($this->sandbox->ensureToStringAllowed(($context["field_name"] ?? null)), [($this->sandbox->ensureToStringAllowed(($context["bundle"] ?? null)) . "__") => ""])));
        // line 3
        $context["bem_element_prefix"] = ((($context["bem_element_prefix"] ?? null)) ? (($context["bem_element_prefix"] ?? null)) : (($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "-")));
        // line 4
        $context["classes"] = ((($context["classes"] ?? null)) ? (($context["classes"] ?? null)) : ([0 =>         // line 5
($context["bem_block"] ?? null)]));
        // line 7
        echo "<a id=\"comments\"></a>
<section";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  ";
        // line 9
        if ((($context["comments"] ?? null) &&  !($context["label_hidden"] ?? null))) {
            // line 10
            echo "    ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo "
    <h2";
            // line 11
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title_attributes"] ?? null), "addClass", [0 => ($this->sandbox->ensureToStringAllowed(($context["bem_element_prefix"] ?? null)) . "title")], "method")), "html", null, true);
            echo "><span>";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t($this->sandbox->ensureToStringAllowed(($context["label"] ?? null))));
            echo "</span></h2>
    ";
            // line 12
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "
  ";
        }
        // line 14
        echo "
  <div class=\"";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_element_prefix"] ?? null)), "html", null, true);
        echo "content\">
    ";
        // line 16
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["comments"] ?? null)), "html", null, true);
        echo "
  </div>

  ";
        // line 19
        if (($context["comment_form"] ?? null)) {
            // line 20
            echo "    <div class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_element_prefix"] ?? null)), "html", null, true);
            echo "form-container\">
      <h2";
            // line 21
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content_attributes"] ?? null), "addClass", [0 => ($this->sandbox->ensureToStringAllowed(($context["bem_element_prefix"] ?? null)) . "form-title")], "method")), "html", null, true);
            echo "><span>";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Add new comment"));
            echo "</span></h2>
      ";
            // line 22
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["comment_form"] ?? null)), "html", null, true);
            echo "
    </div>
  ";
        }
        // line 25
        echo "</section>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/blogger/templates/content/node/blog-article/field--node--field-comments--blog-article.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  119 => 25,  113 => 22,  107 => 21,  102 => 20,  100 => 19,  94 => 16,  90 => 15,  87 => 14,  82 => 12,  76 => 11,  71 => 10,  69 => 9,  65 => 8,  62 => 7,  60 => 5,  59 => 4,  57 => 3,  55 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# ENTITY_TYPE-ENTITY_BUNDLE-VIEW_MODE-FIELD_NAME #}
{% set bem_block = (entity_type ~ '-' ~ bundle ~ (view_mode != 'default' ? '-' ~ view_mode))|clean_class ~ '__' ~ field_name|replace({(bundle ~ '__'): ''})|clean_class %}
{% set bem_element_prefix = bem_element_prefix ?: bem_block ~ '-' %}
{% set classes = classes ?: [
  bem_block,
] %}
<a id=\"comments\"></a>
<section{{ attributes.addClass(classes) }}>
  {% if comments and not label_hidden %}
    {{ title_prefix }}
    <h2{{ title_attributes.addClass(bem_element_prefix ~ 'title') }}><span>{{ label|t }}</span></h2>
    {{ title_suffix }}
  {% endif %}

  <div class=\"{{ bem_element_prefix }}content\">
    {{ comments }}
  </div>

  {% if comment_form %}
    <div class=\"{{ bem_element_prefix }}form-container\">
      <h2{{ content_attributes.addClass(bem_element_prefix ~ 'form-title') }}><span>{{ 'Add new comment'|t }}</span></h2>
      {{ comment_form }}
    </div>
  {% endif %}
</section>
", "themes/custom/blogger/templates/content/node/blog-article/field--node--field-comments--blog-article.html.twig", "/var/www/web/themes/custom/blogger/templates/content/node/blog-article/field--node--field-comments--blog-article.html.twig");
    }
}
