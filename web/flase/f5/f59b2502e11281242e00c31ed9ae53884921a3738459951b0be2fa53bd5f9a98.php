<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/custom/dlog_custom/templates/dlog-previous-next.html.twig */
class __TwigTemplate_ff898dd52ff74957c2fb68ebc73426f9d8f079a37aa5253fe6c779eeb3e5f3a2 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1, "if" => 4];
        $filters = ["escape" => 2, "t" => 6];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if'],
                ['escape', 't'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["bem_block"] = "previous-next";
        // line 2
        echo "<div ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["bem_block"] ?? null)], "method")), "html", null, true);
        echo ">
  <div class=\"";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__previous\">
     ";
        // line 4
        if (($context["previous"] ?? null)) {
            // line 5
            echo "       <a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["previous"] ?? null), "url", [])), "html", null, true);
            echo "\" class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__link ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__link--previous\">
         <span>";
            // line 6
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Previous"));
            echo "</span>
         ";
            // line 7
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["previous"] ?? null), "label", [])), "html", null, true);
            echo "
       </a>
    ";
        }
        // line 10
        echo "  </div>
  <div class=\"";
        // line 11
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__next\">
    ";
        // line 12
        if (($context["next"] ?? null)) {
            // line 13
            echo "      <a href=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["next"] ?? null), "url", [])), "html", null, true);
            echo "\" class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__link ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__link--next\">
        <span>";
            // line 14
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Next"));
            echo "</span>
        ";
            // line 15
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["next"] ?? null), "label", [])), "html", null, true);
            echo "
      </a>
    ";
        }
        // line 18
        echo "  </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "modules/custom/dlog_custom/templates/dlog-previous-next.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  115 => 18,  109 => 15,  105 => 14,  96 => 13,  94 => 12,  90 => 11,  87 => 10,  81 => 7,  77 => 6,  68 => 5,  66 => 4,  62 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% set bem_block  = 'previous-next' %}
<div {{ attributes.addClass(bem_block) }}>
  <div class=\"{{ bem_block  }}__previous\">
     {% if previous %}
       <a href=\"{{ previous.url }}\" class=\"{{ bem_block }}__link {{ bem_block }}__link--previous\">
         <span>{{ 'Previous'| t  }}</span>
         {{ previous.label }}
       </a>
    {% endif %}
  </div>
  <div class=\"{{ bem_block  }}__next\">
    {% if next %}
      <a href=\"{{ next.url }}\" class=\"{{ bem_block }}__link {{ bem_block }}__link--next\">
        <span>{{ 'Next'| t  }}</span>
        {{ next.label }}
      </a>
    {% endif %}
  </div>
</div>

", "modules/custom/dlog_custom/templates/dlog-previous-next.html.twig", "/var/www/web/modules/custom/dlog_custom/templates/dlog-previous-next.html.twig");
    }
}
