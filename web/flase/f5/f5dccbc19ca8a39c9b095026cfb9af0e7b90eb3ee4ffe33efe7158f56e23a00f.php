<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/blogger/templates/theme/dlog-hero--dlog-node-blog-article--image.html.twig */
class __TwigTemplate_04e31a57b99c323e73e37fddfe2d06b79127cbaddda74d1fb61f50edcd8401fd extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1, "if" => 11, "for" => 13];
        $filters = ["escape" => 10, "image_style" => 12, "t" => 16, "raw" => 33];
        $functions = ["file_url" => 14];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'for'],
                ['escape', 'image_style', 't', 'raw'],
                ['file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["bem_block"] = "dlog-hero";
        // line 2
        $context["classes"] = [0 =>         // line 3
($context["bem_block"] ?? null), 1 => (((        // line 4
($context["image"] ?? null) && ($context["video"] ?? null))) ? (($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "-image-and-video")) : ("")), 2 => (((        // line 5
($context["image"] ?? null) &&  !($context["video"] ?? null))) ? (($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "--image")) : ("")), 3 => (( !        // line 6
($context["image"] ?? null)) ? (($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "--plain")) : ("")), 4 => ((        // line 7
($context["subtitle"] ?? null)) ? (($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "--subtile")) : (($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "--no-subtitle")))];
        // line 9
        echo "
<div";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  ";
        // line 11
        if ((($context["image"] ?? null) && ($context["video"] ?? null))) {
            // line 12
            echo "    <video poster=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\twig_tweak\TwigExtension')->imageStyle($this->sandbox->ensureToStringAllowed(($context["image"] ?? null)), "thumbnail"), "html", null, true);
            echo "\" autoplay loop muted class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__video\">
      ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["video"] ?? null));
            foreach ($context['_seq'] as $context["type"] => $context["video_uri"]) {
                // line 14
                echo "        <source src = ";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed($context["video_uri"])]), "html", null, true);
                echo " type=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($context["type"]), "html", null, true);
                echo "\">
      ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['video_uri'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 16
            echo "      ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Your bowser does not support the video tag"));
            echo "
    </video>
  ";
        } elseif (        // line 18
($context["image"] ?? null)) {
            // line 19
            echo "    <img src=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\twig_tweak\TwigExtension')->imageStyle($this->sandbox->ensureToStringAllowed(($context["image"] ?? null)), "dlog_hero"), "html", null, true);
            echo "\" alt=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null)), "html", null, true);
            echo "\" class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__image\">
  ";
        }
        // line 21
        echo "  <div class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__content\">
    ";
        // line 22
        if (($context["tags"] ?? null)) {
            // line 23
            echo "      <div class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__tags\">
        ";
            // line 24
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["tags"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["tag"]) {
                // line 25
                echo "          <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["tag"], "path", [])), "html", null, true);
                echo "\" class=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
                echo "__tag \">
            ";
                // line 26
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["tag"], "label", [])), "html", null, true);
                echo "
          </a>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tag'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "      </div>
    ";
        }
        // line 31
        echo "    <h1 class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__title\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null)), "html", null, true);
        echo "</h1>
    ";
        // line 32
        if (($context["subtitle"] ?? null)) {
            // line 33
            echo "      <div class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__subtitle\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->sandbox->ensureToStringAllowed(($context["subtitle"] ?? null)));
            echo "</div>
    ";
        }
        // line 35
        echo "  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "themes/custom/blogger/templates/theme/dlog-hero--dlog-node-blog-article--image.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 35,  158 => 33,  156 => 32,  149 => 31,  145 => 29,  136 => 26,  129 => 25,  125 => 24,  120 => 23,  118 => 22,  113 => 21,  103 => 19,  101 => 18,  95 => 16,  84 => 14,  80 => 13,  73 => 12,  71 => 11,  67 => 10,  64 => 9,  62 => 7,  61 => 6,  60 => 5,  59 => 4,  58 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% set bem_block = 'dlog-hero' %}
{% set classes = [
  bem_block,
  image and video ? bem_block ~ '-image-and-video',
  image and not video ? bem_block ~ '--image',
  not image ? bem_block ~ '--plain',
  subtitle ? bem_block ~ '--subtile' : bem_block ~ '--no-subtitle'
] %}

<div{{attributes.addClass(classes)}}>
  {% if image and video %}
    <video poster=\"{{ image|image_style('thumbnail') }}\" autoplay loop muted class=\"{{ bem_block }}__video\">
      {% for type, video_uri in video %}
        <source src = {{ file_url(video_uri)  }} type=\"{{ type }}\">
      {% endfor %}
      {{ 'Your bowser does not support the video tag'|t }}
    </video>
  {% elseif image %}
    <img src=\"{{ image|image_style('dlog_hero') }}\" alt=\"{{ title }}\" class=\"{{ bem_block }}__image\">
  {% endif %}
  <div class=\"{{ bem_block }}__content\">
    {% if tags %}
      <div class=\"{{ bem_block }}__tags\">
        {% for tag in tags %}
          <a href=\"{{ tag.path }}\" class=\"{{ bem_block }}__tag \">
            {{ tag.label }}
          </a>
        {% endfor %}
      </div>
    {% endif %}
    <h1 class=\"{{ bem_block }}__title\">{{ title }}</h1>
    {% if subtitle %}
      <div class=\"{{ bem_block }}__subtitle\">{{ subtitle| raw }}</div>
    {% endif %}
  </div>
</div>
", "themes/custom/blogger/templates/theme/dlog-hero--dlog-node-blog-article--image.html.twig", "/var/www/web/themes/custom/blogger/templates/theme/dlog-hero--dlog-node-blog-article--image.html.twig");
    }
}
