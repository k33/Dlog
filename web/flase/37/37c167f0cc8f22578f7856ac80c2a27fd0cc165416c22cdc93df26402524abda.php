<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/content/media/media.html.twig */
class __TwigTemplate_79b1473baa9b0a628645a135f4810fd6668cd57a5ccf9615e0e70526206680a2 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1, "if" => 8, "block" => 9];
        $filters = ["clean_class" => 1, "escape" => 6];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block'],
                ['clean_class', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["bem_block"] = ((($context["bem_block"] ?? null)) ? (($context["bem_block"] ?? null)) : (\Drupal\Component\Utility\Html::getClass((("media-" . $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["media"] ?? null), "bundle", [], "method"))) . (((($context["view_mode"] ?? null) != "default")) ? (("-" . $this->sandbox->ensureToStringAllowed(($context["view_mode"] ?? null)))) : (""))))));
        // line 2
        $context["classes"] = [0 =>         // line 3
($context["bem_block"] ?? null), 1 => (( !$this->getAttribute(        // line 4
($context["media"] ?? null), "isPublished", [], "method")) ? (($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "--unpublished")) : (""))];
        // line 6
        echo "<article";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  ";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title_suffix"] ?? null), "contextual_links", [])), "html", null, true);
        echo "
  ";
        // line 8
        if (($context["content"] ?? null)) {
            // line 9
            echo "    ";
            $this->displayBlock('content', $context, $blocks);
            // line 12
            echo "  ";
        }
        // line 13
        echo "</article>
";
    }

    // line 9
    public function block_content($context, array $blocks = [])
    {
        // line 10
        echo "      ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content"] ?? null)), "html", null, true);
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/content/media/media.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 10,  84 => 9,  79 => 13,  76 => 12,  73 => 9,  71 => 8,  67 => 7,  62 => 6,  60 => 4,  59 => 3,  58 => 2,  56 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% set bem_block = bem_block ?: ('media-' ~ media.bundle() ~ (view_mode != 'default' ? '-' ~ view_mode))|clean_class %}
{% set classes = [
  bem_block,
  not media.isPublished() ? bem_block ~ '--unpublished',
] %}
<article{{ attributes.addClass(classes) }}>
  {{ title_suffix.contextual_links }}
  {% if content %}
    {% block content %}
      {{ content }}
    {% endblock %}
  {% endif %}
</article>
", "themes/contrib/glisseo/templates/content/media/media.html.twig", "/var/www/web/themes/contrib/glisseo/templates/content/media/media.html.twig");
    }
}
