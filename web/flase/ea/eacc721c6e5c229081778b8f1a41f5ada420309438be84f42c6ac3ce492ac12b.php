<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/field/field.html.twig */
class __TwigTemplate_af95bba432d581243fa43b68f074fd65eac23a2af308b71f99f9a2c9ca08c238 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'label' => [$this, 'block_label'],
            'items' => [$this, 'block_items'],
            'item' => [$this, 'block_item'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2, "block" => 13, "if" => 15, "for" => 29];
        $filters = ["clean_class" => 2, "replace" => 2, "escape" => 12];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block', 'if', 'for'],
                ['clean_class', 'replace', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["bem_block"] = ((\Drupal\Component\Utility\Html::getClass(((($this->sandbox->ensureToStringAllowed(($context["entity_type"] ?? null)) . "-") . $this->sandbox->ensureToStringAllowed(($context["bundle"] ?? null))) . (((($context["view_mode"] ?? null) != "default")) ? (("-" . $this->sandbox->ensureToStringAllowed(($context["view_mode"] ?? null)))) : ("")))) . "__") . \Drupal\Component\Utility\Html::getClass(twig_replace_filter($this->sandbox->ensureToStringAllowed(($context["field_name"] ?? null)), [($this->sandbox->ensureToStringAllowed(($context["bundle"] ?? null)) . "__") => ""])));
        // line 3
        $context["bem_element_prefix"] = ((($context["bem_element_prefix"] ?? null)) ? (($context["bem_element_prefix"] ?? null)) : (($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "-")));
        // line 4
        $context["classes"] = ((($context["classes"] ?? null)) ? (($context["classes"] ?? null)) : ([0 =>         // line 5
($context["bem_block"] ?? null), 1 => (((        // line 6
($context["label_display"] ?? null) == "inline")) ? (($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "--label-inline")) : (""))]));
        // line 8
        $context["title_classes"] = [0 => ($this->sandbox->ensureToStringAllowed(        // line 9
($context["bem_element_prefix"] ?? null)) . "label"), 1 => ((        // line 10
($context["label_hidden"] ?? null)) ? ("visually-hidden") : (""))];
        // line 12
        echo "<div";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  ";
        // line 13
        $this->displayBlock('content', $context, $blocks);
        // line 50
        echo "</div>";
    }

    // line 13
    public function block_content($context, array $blocks = [])
    {
        // line 14
        echo "
    ";
        // line 15
        if ( !($context["label_hidden"] ?? null)) {
            // line 16
            echo "      <div";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title_attributes"] ?? null), "addClass", [0 => ($context["title_classes"] ?? null)], "method")), "html", null, true);
            echo ">
        ";
            // line 17
            $this->displayBlock('label', $context, $blocks);
            // line 20
            echo "      </div>
    ";
        }
        // line 22
        echo "
    ";
        // line 23
        $this->displayBlock('items', $context, $blocks);
        // line 48
        echo "
  ";
    }

    // line 17
    public function block_label($context, array $blocks = [])
    {
        // line 18
        echo "          ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null)), "html", null, true);
        echo "
        ";
    }

    // line 23
    public function block_items($context, array $blocks = [])
    {
        // line 24
        echo "      ";
        ob_start();
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content_attributes"] ?? null)), "html", null, true);
        $context["content_attributes_not_empty"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 25
        echo "      ";
        if (((($context["multiple"] ?? null) &&  !($context["label_hidden"] ?? null)) || ($context["content_attributes_not_empty"] ?? null))) {
            // line 26
            echo "        <div";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content_attributes"] ?? null), "addClass", [0 => ($this->sandbox->ensureToStringAllowed(($context["bem_element_prefix"] ?? null)) . "items")], "method")), "html", null, true);
            echo ">
      ";
        }
        // line 28
        echo "
      ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 30
            echo "        ";
            ob_start();
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "attributes", [])), "html", null, true);
            $context["item_attributes_not_empty"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 31
            echo "        ";
            if ((($context["multiple"] ?? null) || ($context["item_attributes_not_empty"] ?? null))) {
                // line 32
                echo "          <div";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "addClass", [0 => ($this->sandbox->ensureToStringAllowed(($context["bem_element_prefix"] ?? null)) . "item")], "method")), "html", null, true);
                echo ">
        ";
            }
            // line 35
            $this->displayBlock('item', $context, $blocks);
            // line 38
            echo "
        ";
            // line 39
            if ((($context["multiple"] ?? null) || ($context["item_attributes_not_empty"] ?? null))) {
                // line 40
                echo "          </div>
        ";
            }
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "
      ";
        // line 44
        if (((($context["multiple"] ?? null) &&  !($context["label_hidden"] ?? null)) || ($context["content_attributes_not_empty"] ?? null))) {
            // line 45
            echo "        </div>
      ";
        }
        // line 47
        echo "    ";
    }

    // line 35
    public function block_item($context, array $blocks = [])
    {
        // line 36
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["item"] ?? null), "content", [])), "html", null, true);
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/field/field.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  211 => 36,  208 => 35,  204 => 47,  200 => 45,  198 => 44,  195 => 43,  179 => 40,  177 => 39,  174 => 38,  172 => 35,  166 => 32,  163 => 31,  158 => 30,  141 => 29,  138 => 28,  132 => 26,  129 => 25,  124 => 24,  121 => 23,  114 => 18,  111 => 17,  106 => 48,  104 => 23,  101 => 22,  97 => 20,  95 => 17,  90 => 16,  88 => 15,  85 => 14,  82 => 13,  78 => 50,  76 => 13,  71 => 12,  69 => 10,  68 => 9,  67 => 8,  65 => 6,  64 => 5,  63 => 4,  61 => 3,  59 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# ENTITY_TYPE-ENTITY_BUNDLE-VIEW_MODE-FIELD_NAME #}
{% set bem_block = (entity_type ~ '-' ~ bundle ~ (view_mode != 'default' ? '-' ~ view_mode))|clean_class ~ '__' ~ field_name|replace({(bundle ~ '__'): ''})|clean_class %}
{% set bem_element_prefix = bem_element_prefix ?: bem_block ~ '-' %}
{% set classes = classes ?: [
  bem_block,
  label_display == 'inline' ? bem_block ~ '--label-inline',
] %}
{% set title_classes = [
  bem_element_prefix ~ 'label',
  label_hidden ? 'visually-hidden',
] %}
<div{{ attributes.addClass(classes) }}>
  {% block content %}

    {% if not label_hidden %}
      <div{{ title_attributes.addClass(title_classes) }}>
        {% block label %}
          {{ label }}
        {% endblock %}
      </div>
    {% endif %}

    {% block items %}
      {% set content_attributes_not_empty -%}{{ content_attributes }}{%- endset %}
      {% if (multiple and not label_hidden) or content_attributes_not_empty %}
        <div{{ content_attributes.addClass(bem_element_prefix ~ 'items') }}>
      {% endif %}

      {% for item in items %}
        {% set item_attributes_not_empty -%}{{ item.attributes }}{%- endset %}
        {% if multiple or item_attributes_not_empty %}
          <div{{ item.attributes.addClass(bem_element_prefix ~ 'item') }}>
        {% endif -%}

        {% block item %}
          {{- item.content -}}
        {% endblock %}

        {% if multiple or item_attributes_not_empty %}
          </div>
        {% endif -%}
      {% endfor %}

      {% if (multiple and not label_hidden) or content_attributes_not_empty %}
        </div>
      {% endif %}
    {% endblock %}

  {% endblock %}
</div>", "themes/contrib/glisseo/templates/field/field.html.twig", "/var/www/web/themes/contrib/glisseo/templates/field/field.html.twig");
    }
}
