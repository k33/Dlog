<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* block.html.twig */
class __TwigTemplate_bd022d2ff4e71be5b245e7d9f60c66099907a98e3aecd458a0f38b1d4fe278cf extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["do" => 9, "set" => 10, "for" => 15, "if" => 22, "block" => 24];
        $filters = ["clean_class" => 10, "first" => 10, "split" => 10, "slice" => 14, "merge" => 16, "escape" => 19];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['do', 'set', 'for', 'if', 'block'],
                ['clean_class', 'first', 'split', 'slice', 'merge', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 9
        $this->getAttribute(($context["attributes"] ?? null), "removeAttribute", [0 => "id"], "method");
        // line 10
        $context["bem_block"] = ((($context["bem_block"] ?? null)) ? (($context["bem_block"] ?? null)) : (("block-" . \Drupal\Component\Utility\Html::getClass(twig_first($this->env, twig_split_filter($this->env, $this->sandbox->ensureToStringAllowed(($context["block_id"] ?? null)), "__", 2))))));
        // line 11
        $context["classes"] = [0 =>         // line 12
($context["bem_block"] ?? null)];
        // line 14
        $context["bem_modifiers"] = ((($context["bem_modifiers"] ?? null)) ? (($context["bem_modifiers"] ?? null)) : (twig_slice($this->env, twig_split_filter($this->env, $this->sandbox->ensureToStringAllowed(($context["block_id"] ?? null)), "__"), 1)));
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["bem_modifiers"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["modifier"]) {
            // line 16
            echo "  ";
            $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null)), [0 => (($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "--") . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($context["modifier"])))]);
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['modifier'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "
<div";
        // line 19
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">

  ";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
  ";
        // line 22
        if (($context["label"] ?? null)) {
            // line 23
            echo "    <div";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title_attributes"] ?? null), "addClass", [0 => ($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "__title")], "method")), "html", null, true);
            echo ">
      ";
            // line 24
            $this->displayBlock('title', $context, $blocks);
            // line 27
            echo "    </div>
  ";
        }
        // line 29
        echo "  ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "

  <div";
        // line 31
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content_attributes"] ?? null), "addClass", [0 => ($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "__content")], "method")), "html", null, true);
        echo ">
    ";
        // line 32
        $this->displayBlock('content', $context, $blocks);
        // line 35
        echo "  </div>

</div>
";
    }

    // line 24
    public function block_title($context, array $blocks = [])
    {
        // line 25
        echo "        ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["label"] ?? null)), "html", null, true);
        echo "
      ";
    }

    // line 32
    public function block_content($context, array $blocks = [])
    {
        // line 33
        echo "      ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content"] ?? null)), "html", null, true);
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 33,  131 => 32,  124 => 25,  121 => 24,  114 => 35,  112 => 32,  108 => 31,  102 => 29,  98 => 27,  96 => 24,  91 => 23,  89 => 22,  85 => 21,  80 => 19,  77 => 18,  70 => 16,  66 => 15,  64 => 14,  62 => 12,  61 => 11,  59 => 10,  57 => 9,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{#
/**
 * @file
 * Theme override to display a block.
 *
 * @see template_preprocess_block()
 */
#}
{% do attributes.removeAttribute('id') %}
{% set bem_block = bem_block ?: 'block-' ~ block_id|split('__', 2)|first|clean_class %}
{% set classes = [
  bem_block,
] %}
{% set bem_modifiers = bem_modifiers ?: block_id|split('__')|slice(1) %}
{% for modifier in bem_modifiers %}
  {% set classes = classes|merge([bem_block ~ '--' ~ modifier|clean_class]) %}
{% endfor %}

<div{{ attributes.addClass(classes) }}>

  {{ title_prefix }}
  {% if label %}
    <div{{ title_attributes.addClass(bem_block ~ '__title') }}>
      {% block title %}
        {{ label }}
      {% endblock title %}
    </div>
  {% endif %}
  {{ title_suffix }}

  <div{{ content_attributes.addClass(bem_block ~ '__content') }}>
    {% block content %}
      {{ content }}
    {% endblock content %}
  </div>

</div>
", "block.html.twig", "themes/contrib/glisseo/templates/content/block/block.html.twig");
    }
}
