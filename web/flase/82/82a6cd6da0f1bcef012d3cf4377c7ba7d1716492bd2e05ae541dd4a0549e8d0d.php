<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @blogger/include/footer.html.twig */
class __TwigTemplate_cb6f5d73d4aa65855a61f7ecc5ed1284418be47998da137e541b69a1a56dbb50 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["import" => 1, "set" => 2];
        $filters = ["escape" => 3, "date" => 8];
        $functions = ["drupal_block" => 7];

        try {
            $this->sandbox->checkSecurity(
                ['import', 'set'],
                ['escape', 'date'],
                ['drupal_block']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["social_links"] = $this->loadTemplate("@blogger/include/social-links.html.twig", "@blogger/include/footer.html.twig", 1)->unwrap();
        // line 2
        $context["bem_block"] = "footer";
        // line 3
        echo "<footer class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "\">
  <div class=\"";
        // line 4
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__content\">
    <div class=\"";
        // line 5
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__copyright\">
      ";
        // line 6
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["social_links"]->getlinks("footer-social"));
        echo "
     ";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\twig_tweak\TwigExtension')->drupalBlock("system_menu_block:footer"), "html", null, true);
        echo "
      &copy; ";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " <strong>Dlog Hero</strong>. Все права защищени.
    </div>
    <a href=\"#\" class=\"";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__top\">Вверх</a>
  </div>
</footer>
";
    }

    public function getTemplateName()
    {
        return "@blogger/include/footer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 10,  80 => 8,  76 => 7,  72 => 6,  68 => 5,  64 => 4,  59 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% import '@blogger/include/social-links.html.twig' as social_links %}
{% set bem_block = \"footer\" %}
<footer class=\"{{bem_block}}\">
  <div class=\"{{bem_block}}__content\">
    <div class=\"{{bem_block}}__copyright\">
      {{ social_links.links('footer-social') }}
     {{ drupal_block('system_menu_block:footer') }}
      &copy; {{'now'|date('Y')}} <strong>Dlog Hero</strong>. Все права защищени.
    </div>
    <a href=\"#\" class=\"{{bem_block}}__top\">Вверх</a>
  </div>
</footer>
", "@blogger/include/footer.html.twig", "/var/www/web/themes/custom/blogger/templates/include/footer.html.twig");
    }
}
