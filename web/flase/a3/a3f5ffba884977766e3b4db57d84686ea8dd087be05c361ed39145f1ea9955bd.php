<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__f9eb6b9ea47f9d2f96222d73805fe0254a631c13ee7a3ec2f825f4a4fa147d9c */
class __TwigTemplate_0b1d23e5bf731aa999595efe1821dbd9cc80307762e3cb94819eebf5d3ba4c0d extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1];
        $filters = ["escape" => 10];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["bem_block"] = ((($context["bem_block"] ?? null)) ? (($context["bem_block"] ?? null)) : ("social-links"));
        // line 2
        $context["socila_items"] = ["facebook" => "#", "vk" => "#", "twitter" => "#", "instagram" => "#", "youtube" => "#", "linkedin" => "#"];
        // line 10
        echo "<div class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "\">
  <ul class=\"";
        // line 11
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__items\"><li class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__item\"><a href=\"#\">Test</a></li>
  </ul></div>
";
    }

    public function getTemplateName()
    {
        return "__string_template__f9eb6b9ea47f9d2f96222d73805fe0254a631c13ee7a3ec2f825f4a4fa147d9c";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 11,  59 => 10,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# inline_template_start #}{% set bem_block = bem_block ?: \"social-links\" %}
{% set socila_items = {
  'facebook':'#',
  'vk' : '#',
  'twitter': '#',
  'instagram':'#',
  'youtube': '#',
  'linkedin': '#'
} %}
<div class=\"{{bem_block}}\">
  <ul class=\"{{bem_block}}__items\"><li class=\"{{bem_block}}__item\"><a href=\"#\">Test</a></li>
  </ul></div>
", "__string_template__f9eb6b9ea47f9d2f96222d73805fe0254a631c13ee7a3ec2f825f4a4fa147d9c", "");
    }
}
