<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/navigation/menu.html.twig */
class __TwigTemplate_ea51e94d348b7a3f6d64995669824576280afe69212512c7553f79fa0fe41d01 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["import" => 1, "if" => 8, "set" => 9, "do" => 10, "macro" => 15, "for" => 20];
        $filters = ["escape" => 19];
        $functions = ["link" => 28];

        try {
            $this->sandbox->checkSecurity(
                ['import', 'if', 'set', 'do', 'macro', 'for'],
                ['escape'],
                ['link']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["menus"] = $this;
        // line 2
        echo "
";
        // line 7
        echo "
";
        // line 8
        if ($this->getAttribute(($context["attributes"] ?? null), "bem_base", [])) {
            // line 9
            echo "  ";
            $context["bem_base"] = $this->getAttribute(($context["attributes"] ?? null), "bem_base", []);
            // line 10
            echo "  ";
            $this->getAttribute(($context["attributes"] ?? null), "removeAttribute", [0 => "bem_base"], "method");
        }
        // line 12
        echo "
";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links(($context["items"] ?? null), ($context["attributes"] ?? null), 0, ($context["bem_base"] ?? null)));
        echo "

";
    }

    // line 15
    public function getmenu_links($__items__ = null, $__attributes__ = null, $__menu_level__ = null, $__bem_base__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals([
            "items" => $__items__,
            "attributes" => $__attributes__,
            "menu_level" => $__menu_level__,
            "bem_base" => $__bem_base__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start();
        try {
            // line 16
            echo "  ";
            $context["menus"] = $this;
            // line 17
            echo "  ";
            if (($context["items"] ?? null)) {
                // line 18
                echo "    ";
                if ((($context["menu_level"] ?? null) == 0)) {
                    // line 19
                    echo "      <ul";
                    echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => [0 => ((($context["bem_base"] ?? null)) ? (($this->sandbox->ensureToStringAllowed(($context["bem_base"] ?? null)) . "menu")) : (""))]], "method")), "html", null, true);
                    echo ">
        ";
                    // line 20
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                        // line 21
                        echo "          ";
                        $context["classes"] = [0 => ($this->sandbox->ensureToStringAllowed(                        // line 22
($context["bem_base"] ?? null)) . "menu-item"), 1 => (($this->getAttribute(                        // line 23
$context["item"], "is_expanded", [])) ? (($this->sandbox->ensureToStringAllowed(($context["bem_base"] ?? null)) . "menu-item--expanded")) : ("")), 2 => (($this->getAttribute(                        // line 24
$context["item"], "is_collapsed", [])) ? (($this->sandbox->ensureToStringAllowed(($context["bem_base"] ?? null)) . "menu-item--collapsed")) : ("")), 3 => (($this->getAttribute(                        // line 25
$context["item"], "in_active_trail", [])) ? (($this->sandbox->ensureToStringAllowed(($context["bem_base"] ?? null)) . "menu-item--active-trail")) : (""))];
                        // line 27
                        echo "          <li";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
                        echo ">
            ";
                        // line 28
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getLink($this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "title", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "url", [])), ["class" => [0 => ((($context["bem_base"] ?? null)) ? (($this->sandbox->ensureToStringAllowed(($context["bem_base"] ?? null)) . "menu-item-link")) : (""))]]), "html", null, true);
                        echo "
            ";
                        // line 29
                        if ($this->getAttribute($context["item"], "below", [])) {
                            // line 30
                            echo "              ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links($this->getAttribute($context["item"], "below", []), ($context["attributes"] ?? null), (($context["menu_level"] ?? null) + 1), ($context["bem_base"] ?? null)));
                            echo "
            ";
                        }
                        // line 32
                        echo "          </li>
        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 34
                    echo "      </ul>
    ";
                } else {
                    // line 36
                    echo "      <ul class=\"";
                    if (($context["bem_base"] ?? null)) {
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_base"] ?? null)), "html", null, true);
                        echo "submenu";
                    }
                    echo "\">
        ";
                    // line 37
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                        // line 38
                        echo "          ";
                        $context["classes"] = [0 => ($this->sandbox->ensureToStringAllowed(                        // line 39
($context["bem_base"] ?? null)) . "submenu-item"), 1 => (($this->getAttribute(                        // line 40
$context["item"], "is_expanded", [])) ? (($this->sandbox->ensureToStringAllowed(($context["bem_base"] ?? null)) . "submenu-item--expanded")) : ("")), 2 => (($this->getAttribute(                        // line 41
$context["item"], "is_collapsed", [])) ? (($this->sandbox->ensureToStringAllowed(($context["bem_base"] ?? null)) . "submenu-item--collapsed")) : ("")), 3 => (($this->getAttribute(                        // line 42
$context["item"], "in_active_trail", [])) ? (($this->sandbox->ensureToStringAllowed(($context["bem_base"] ?? null)) . "submenu-item--active-trail")) : (""))];
                        // line 44
                        echo "          <li";
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute($context["item"], "attributes", []), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
                        echo ">
            ";
                        // line 45
                        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getLink($this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "title", [])), $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "url", [])), ["class" => [0 => ((($context["bem_base"] ?? null)) ? (($this->sandbox->ensureToStringAllowed(($context["bem_base"] ?? null)) . "submenu-item-link")) : (""))]]), "html", null, true);
                        echo "
            ";
                        // line 46
                        if ($this->getAttribute($context["item"], "below", [])) {
                            // line 47
                            echo "              ";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($context["menus"]->getmenu_links($this->getAttribute($context["item"], "below", []), ($context["attributes"] ?? null), (($context["menu_level"] ?? null) + 1), ($context["bem_base"] ?? null)));
                            echo "
            ";
                        }
                        // line 49
                        echo "          </li>
        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 51
                    echo "      </ul>
    ";
                }
                // line 53
                echo "  ";
            }
        } catch (\Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (\Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/navigation/menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 53,  193 => 51,  186 => 49,  180 => 47,  178 => 46,  174 => 45,  169 => 44,  167 => 42,  166 => 41,  165 => 40,  164 => 39,  162 => 38,  158 => 37,  150 => 36,  146 => 34,  139 => 32,  133 => 30,  131 => 29,  127 => 28,  122 => 27,  120 => 25,  119 => 24,  118 => 23,  117 => 22,  115 => 21,  111 => 20,  106 => 19,  103 => 18,  100 => 17,  97 => 16,  82 => 15,  75 => 13,  72 => 12,  68 => 10,  65 => 9,  63 => 8,  60 => 7,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% import _self as menus %}

{#
  We call a macro which calls itself to render the full tree.
  @see http://twig.sensiolabs.org/doc/tags/macro.html
#}

{% if attributes.bem_base %}
  {% set bem_base = attributes.bem_base %}
  {% do attributes.removeAttribute('bem_base') %}
{% endif %}

{{ menus.menu_links(items, attributes, 0, bem_base) }}

{% macro menu_links(items, attributes, menu_level, bem_base) %}
  {% import _self as menus %}
  {% if items %}
    {% if menu_level == 0 %}
      <ul{{ attributes.addClass([bem_base ? bem_base ~ 'menu']) }}>
        {% for item in items %}
          {% set classes = [
            bem_base ~ 'menu-item',
            item.is_expanded ? bem_base ~ 'menu-item--expanded',
            item.is_collapsed ? bem_base ~ 'menu-item--collapsed',
            item.in_active_trail ? bem_base ~ 'menu-item--active-trail',
          ] %}
          <li{{ item.attributes.addClass(classes) }}>
            {{ link(item.title, item.url, { 'class': [bem_base ? bem_base ~ 'menu-item-link'] }) }}
            {% if item.below %}
              {{ menus.menu_links(item.below, attributes, menu_level + 1, bem_base) }}
            {% endif %}
          </li>
        {% endfor %}
      </ul>
    {% else %}
      <ul class=\"{% if bem_base %}{{ bem_base }}submenu{% endif %}\">
        {% for item in items %}
          {% set classes = [
            bem_base ~ 'submenu-item',
            item.is_expanded ? bem_base ~ 'submenu-item--expanded',
            item.is_collapsed ? bem_base ~ 'submenu-item--collapsed',
            item.in_active_trail ? bem_base ~ 'submenu-item--active-trail',
          ] %}
          <li{{ item.attributes.addClass(classes) }}>
            {{ link(item.title, item.url, { 'class': [bem_base ? bem_base ~ 'submenu-item-link'] }) }}
            {% if item.below %}
              {{ menus.menu_links(item.below, attributes, menu_level + 1, bem_base) }}
            {% endif %}
          </li>
        {% endfor %}
      </ul>
    {% endif %}
  {% endif %}
{% endmacro %}
", "themes/contrib/glisseo/templates/navigation/menu.html.twig", "/var/www/web/themes/contrib/glisseo/templates/navigation/menu.html.twig");
    }
}
