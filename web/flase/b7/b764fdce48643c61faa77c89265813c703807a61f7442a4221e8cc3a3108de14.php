<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/content/user/user.html.twig */
class __TwigTemplate_82b3e7998877addba027bbfe4093715ba0156ff88d03b091df4be92298318485 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1, "if" => 7, "block" => 8];
        $filters = ["clean_class" => 1, "escape" => 6];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block'],
                ['clean_class', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["bem_block"] = ((($context["bem_block"] ?? null)) ? (($context["bem_block"] ?? null)) : (\Drupal\Component\Utility\Html::getClass((("user-" . $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["user"] ?? null), "bundle", []))) . (((($context["view_mode"] ?? null) != "default")) ? (("-" . $this->sandbox->ensureToStringAllowed(($context["view_mode"] ?? null)))) : (""))))));
        // line 2
        $context["classes"] = [0 =>         // line 3
($context["bem_block"] ?? null), 1 => (($this->getAttribute(        // line 4
($context["user"] ?? null), "isBlocked", [], "method")) ? (($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "--blocked")) : (""))];
        // line 6
        echo "<article";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  ";
        // line 7
        if (($context["content"] ?? null)) {
            // line 8
            echo "    ";
            $this->displayBlock('content', $context, $blocks);
            // line 11
            echo "  ";
        }
        // line 12
        echo "</article>
";
    }

    // line 8
    public function block_content($context, array $blocks = [])
    {
        // line 9
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content"] ?? null)), "html", null, true);
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/content/user/user.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 9,  80 => 8,  75 => 12,  72 => 11,  69 => 8,  67 => 7,  62 => 6,  60 => 4,  59 => 3,  58 => 2,  56 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% set bem_block = bem_block ?: ('user-' ~ user.bundle ~ (view_mode != 'default' ? '-' ~ view_mode))|clean_class %}
{% set classes = [
  bem_block,
  user.isBlocked() ? bem_block ~ '--blocked',
] %}
<article{{ attributes.addClass(classes) }}>
  {% if content %}
    {% block content %}
      {{- content -}}
    {% endblock %}
  {% endif %}
</article>
", "themes/contrib/glisseo/templates/content/user/user.html.twig", "/var/www/web/themes/contrib/glisseo/templates/content/user/user.html.twig");
    }
}
