<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/navigation/breadcrumb.html.twig */
class __TwigTemplate_bf1bd5016f7319b8072d30dad1d7d565d655bd85088e617b486e8434d1f5a367 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 1, "spaceless" => 4, "for" => 7, "set" => 19];
        $filters = ["t" => 3, "length" => 5, "escape" => 10, "last" => 19];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'spaceless', 'for', 'set'],
                ['t', 'length', 'escape', 'last'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        if (($context["breadcrumb"] ?? null)) {
            // line 2
            echo "  <nav class=\"breadcrumbs\" aria-labelledby=\"system-breadcrumb\">
    <h2 class=\"visually-hidden\">";
            // line 3
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Breadcrumb"));
            echo "</h2>
    ";
            // line 4
            ob_start();
            // line 5
            echo "      ";
            if ((twig_length_filter($this->env, ($context["breadcrumb"] ?? null)) > 1)) {
                // line 6
                echo "        <div class=\"breadcrumbs__path\">
          ";
                // line 7
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["breadcrumb"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 8
                    echo "            ";
                    if ( !$this->getAttribute($context["loop"], "last", [])) {
                        // line 9
                        echo "              ";
                        if ($this->getAttribute($context["item"], "url", [])) {
                            // line 10
                            echo "                <a href=\"";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "url", [])), "html", null, true);
                            echo "\" class=\"breadcrumbs__item-link\">";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "text", [])), "html", null, true);
                            echo "</a>
              ";
                        } else {
                            // line 12
                            echo "                <span class=\"breadcrumbs__item-link breadcrumbs__item-link--text\">";
                            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($context["item"], "text", [])), "html", null, true);
                            echo "</span>
              ";
                        }
                        // line 14
                        echo "            ";
                    }
                    // line 15
                    echo "          ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 16
                echo "        </div>
      ";
            }
            // line 18
            echo "      <div class=\"breadcrumbs__current\">
        ";
            // line 19
            $context["last"] = twig_last($this->env, $this->sandbox->ensureToStringAllowed(($context["breadcrumb"] ?? null)));
            // line 20
            echo "        ";
            if ($this->getAttribute(($context["last"] ?? null), "url", [])) {
                // line 21
                echo "          <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["last"] ?? null), "url", [])), "html", null, true);
                echo "\" class=\"breadcrumbs__item-link is-current\">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["last"] ?? null), "text", [])), "html", null, true);
                echo "</a>
        ";
            } else {
                // line 23
                echo "          <span class=\"breadcrumbs__item-link breadcrumbs__item-link--text is-current\">";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["last"] ?? null), "text", [])), "html", null, true);
                echo "</span>
        ";
            }
            // line 25
            echo "      </div>
    ";
            echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
            // line 27
            echo "  </nav>
";
        }
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/navigation/breadcrumb.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 27,  152 => 25,  146 => 23,  138 => 21,  135 => 20,  133 => 19,  130 => 18,  126 => 16,  112 => 15,  109 => 14,  103 => 12,  95 => 10,  92 => 9,  89 => 8,  72 => 7,  69 => 6,  66 => 5,  64 => 4,  60 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% if breadcrumb %}
  <nav class=\"breadcrumbs\" aria-labelledby=\"system-breadcrumb\">
    <h2 class=\"visually-hidden\">{{ 'Breadcrumb'|t }}</h2>
    {% spaceless %}
      {% if breadcrumb|length > 1 %}
        <div class=\"breadcrumbs__path\">
          {% for item in breadcrumb %}
            {% if not loop.last %}
              {% if item.url %}
                <a href=\"{{ item.url }}\" class=\"breadcrumbs__item-link\">{{ item.text }}</a>
              {% else %}
                <span class=\"breadcrumbs__item-link breadcrumbs__item-link--text\">{{ item.text }}</span>
              {% endif %}
            {% endif %}
          {% endfor %}
        </div>
      {% endif %}
      <div class=\"breadcrumbs__current\">
        {% set last = breadcrumb|last %}
        {% if last.url %}
          <a href=\"{{ last.url }}\" class=\"breadcrumbs__item-link is-current\">{{ last.text }}</a>
        {% else %}
          <span class=\"breadcrumbs__item-link breadcrumbs__item-link--text is-current\">{{ last.text }}</span>
        {% endif %}
      </div>
    {% endspaceless %}
  </nav>
{% endif %}
", "themes/contrib/glisseo/templates/navigation/breadcrumb.html.twig", "/var/www/web/themes/contrib/glisseo/templates/navigation/breadcrumb.html.twig");
    }
}
