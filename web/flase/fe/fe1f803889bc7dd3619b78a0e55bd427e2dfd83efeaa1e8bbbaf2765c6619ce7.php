<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/content/block/block--system-menu-block.html.twig */
class __TwigTemplate_63cce94211ce5b8931192573aa8444344d8fad401c4cce4949821f064f379461 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1, "for" => 7, "if" => 12, "do" => 13, "block" => 25];
        $filters = ["clean_class" => 1, "first" => 1, "split" => 1, "slice" => 2, "merge" => 8, "clean_id" => 10, "escape" => 21];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'for', 'if', 'do', 'block'],
                ['clean_class', 'first', 'split', 'slice', 'merge', 'clean_id', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["bem_block"] = ("block-" . \Drupal\Component\Utility\Html::getClass(twig_first($this->env, twig_split_filter($this->env, $this->sandbox->ensureToStringAllowed(($context["block_id"] ?? null)), "__", 2))));
        // line 2
        $context["bem_modifiers"] = twig_slice($this->env, twig_split_filter($this->env, $this->sandbox->ensureToStringAllowed(($context["block_id"] ?? null)), "__"), 1);
        // line 3
        $context["classes"] = [0 =>         // line 4
($context["bem_block"] ?? null)];
        // line 6
        echo "
";
        // line 7
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, twig_split_filter($this->env, ($context["block_id"] ?? null), "__"), 1));
        foreach ($context['_seq'] as $context["_key"] => $context["modifier"]) {
            // line 8
            echo "  ";
            $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null)), [0 => (($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "--") . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed($context["modifier"])))]);
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['modifier'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 10
        $context["heading_id"] = ($this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "id", [])) . \Drupal\Component\Utility\Html::getId("-title"));
        // line 12
        if ( !$this->getAttribute(($context["configuration"] ?? null), "label_display", [])) {
            // line 13
            echo "  ";
            $this->getAttribute(($context["title_attributes"] ?? null), "addClass", [0 => "visually-hidden"], "method");
        }
        // line 15
        echo "
";
        // line 16
        $this->getAttribute(($context["attributes"] ?? null), "removeAttribute", [0 => "id"], "method");
        // line 17
        $this->getAttribute(($context["attributes"] ?? null), "setAttribute", [0 => "aria-labelledby", 1 => ($context["heading_id"] ?? null)], "method");
        // line 18
        $this->getAttribute(($context["title_attributes"] ?? null), "setAttribute", [0 => "id", 1 => ($context["heading_id"] ?? null)], "method");
        // line 19
        $this->getAttribute(($context["content_attributes"] ?? null), "setAttribute", [0 => "bem_base", 1 => (($context["bem_block"] ?? null) . "__")], "method");
        // line 20
        echo "
<nav";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">

  ";
        // line 23
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
  <h2";
        // line 24
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title_attributes"] ?? null), "addClass", [0 => ($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "__title")], "method")), "html", null, true);
        echo ">
    ";
        // line 25
        $this->displayBlock('title', $context, $blocks);
        // line 28
        echo "  </h2>
  ";
        // line 29
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "

  ";
        // line 31
        $this->displayBlock('content', $context, $blocks);
        // line 34
        echo "
</nav>
";
    }

    // line 25
    public function block_title($context, array $blocks = [])
    {
        // line 26
        echo "      ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["configuration"] ?? null), "label", [])), "html", null, true);
        echo "
    ";
    }

    // line 31
    public function block_content($context, array $blocks = [])
    {
        // line 32
        echo "    ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content"] ?? null)), "html", null, true);
        echo "
  ";
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/content/block/block--system-menu-block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 32,  141 => 31,  134 => 26,  131 => 25,  125 => 34,  123 => 31,  118 => 29,  115 => 28,  113 => 25,  109 => 24,  105 => 23,  100 => 21,  97 => 20,  95 => 19,  93 => 18,  91 => 17,  89 => 16,  86 => 15,  82 => 13,  80 => 12,  78 => 10,  71 => 8,  67 => 7,  64 => 6,  62 => 4,  61 => 3,  59 => 2,  57 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% set bem_block = 'block-' ~ block_id|split('__', 2)|first|clean_class %}
{% set bem_modifiers = block_id|split('__')|slice(1) %}
{% set classes = [
  bem_block,
] %}

{% for modifier in block_id|split('__')|slice(1) %}
  {% set classes = classes|merge([bem_block ~ '--' ~ modifier|clean_class]) %}
{% endfor %}
{% set heading_id = attributes.id ~ '-title'|clean_id %}
{# Label. If not displayed, we still provide it for screen readers. #}
{% if not configuration.label_display %}
  {% do title_attributes.addClass('visually-hidden') %}
{% endif %}

{% do attributes.removeAttribute('id') %}
{% do attributes.setAttribute('aria-labelledby', heading_id) %}
{% do title_attributes.setAttribute('id', heading_id) %}
{% do content_attributes.setAttribute('bem_base', bem_block ~ '__') %}

<nav{{ attributes.addClass(classes) }}>

  {{ title_prefix }}
  <h2{{ title_attributes.addClass(bem_block ~ '__title') }}>
    {% block title %}
      {{ configuration.label }}
    {% endblock title %}
  </h2>
  {{ title_suffix }}

  {% block content %}
    {{ content }}
  {% endblock %}

</nav>
", "themes/contrib/glisseo/templates/content/block/block--system-menu-block.html.twig", "/var/www/web/themes/contrib/glisseo/templates/content/block/block--system-menu-block.html.twig");
    }
}
