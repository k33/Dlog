<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/content/taxonomy-term/taxonomy-term.html.twig */
class __TwigTemplate_47dafe78dee9d610ed7d41836059e768d303348a1a1d18bdf1aa6e4ee6a5f6b2 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1, "if" => 8, "block" => 10];
        $filters = ["clean_class" => 1, "escape" => 5];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'if', 'block'],
                ['clean_class', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["bem_block"] = ((($context["bem_block"] ?? null)) ? (($context["bem_block"] ?? null)) : (\Drupal\Component\Utility\Html::getClass((("taxonomy-term-" . $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["term"] ?? null), "bundle", []))) . (((($context["view_mode"] ?? null) != "default")) ? (("-" . $this->sandbox->ensureToStringAllowed(($context["view_mode"] ?? null)))) : (""))))));
        // line 2
        $context["classes"] = [0 =>         // line 3
($context["bem_block"] ?? null)];
        // line 5
        echo "<div";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  ";
        // line 6
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "

  ";
        // line 8
        if ( !($context["page"] ?? null)) {
            // line 9
            echo "    <h2 class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
            echo "__title\">
      ";
            // line 10
            $this->displayBlock('title', $context, $blocks);
            // line 13
            echo "    </h2>
  ";
        }
        // line 15
        echo "
  ";
        // line 16
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "

  <div class=\"";
        // line 18
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__content\">
    ";
        // line 19
        $this->displayBlock('content', $context, $blocks);
        // line 22
        echo "  </div>
</div>
";
    }

    // line 10
    public function block_title($context, array $blocks = [])
    {
        // line 11
        echo "        <a href=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["url"] ?? null)), "html", null, true);
        echo "\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["name"] ?? null)), "html", null, true);
        echo "</a>
      ";
    }

    // line 19
    public function block_content($context, array $blocks = [])
    {
        // line 20
        echo "      ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content"] ?? null)), "html", null, true);
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/content/taxonomy-term/taxonomy-term.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 20,  117 => 19,  108 => 11,  105 => 10,  99 => 22,  97 => 19,  93 => 18,  88 => 16,  85 => 15,  81 => 13,  79 => 10,  74 => 9,  72 => 8,  67 => 6,  62 => 5,  60 => 3,  59 => 2,  57 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% set bem_block = bem_block ?: ('taxonomy-term-' ~ term.bundle ~ (view_mode != 'default' ? '-' ~ view_mode))|clean_class %}
{% set classes = [
  bem_block,
] %}
<div{{ attributes.addClass(classes) }}>
  {{ title_prefix }}

  {% if not page %}
    <h2 class=\"{{ bem_block }}__title\">
      {% block title %}
        <a href=\"{{ url }}\">{{ name }}</a>
      {% endblock %}
    </h2>
  {% endif %}

  {{ title_suffix }}

  <div class=\"{{ bem_block }}__content\">
    {% block content %}
      {{ content }}
    {% endblock %}
  </div>
</div>
", "themes/contrib/glisseo/templates/content/taxonomy-term/taxonomy-term.html.twig", "/var/www/web/themes/contrib/glisseo/templates/content/taxonomy-term/taxonomy-term.html.twig");
    }
}
