<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @blogger/include/pages/front-page.html.twig */
class __TwigTemplate_8b74b2faa39df585ec6926d4c9272cfb365cce28d8a0217c4a46507efe6bfaf2 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1];
        $filters = ["escape" => 2, "image_style" => 4, "t" => 6];
        $functions = ["file_url" => 5, "url" => 41, "drupal_view" => 50];

        try {
            $this->sandbox->checkSecurity(
                ['set'],
                ['escape', 'image_style', 't'],
                ['file_url', 'url', 'drupal_view']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["bem_block"] = "front-page";
        // line 2
        echo "<main class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "\">
  <div class=\"";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__welcome\">
    <video poster=\"";
        // line 4
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\twig_tweak\TwigExtension')->imageStyle($this->sandbox->ensureToStringAllowed(($context["search_image_uri"] ?? null)), "thumbnail"), "html", null, true);
        echo "\" autoplay loop muted class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__welcome-video\">
        <source src = \"";
        // line 5
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed(($context["search_video_uri"] ?? null))]), "html", null, true);
        echo "\" type=\"video/mp4\">
      ";
        // line 6
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Your bowser does not support the video tag"));
        echo "
    </video>
    <div class=\"";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__container\">
      <div class=\"";
        // line 9
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__welcome-inner\">
        <h1 class=\"";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__welcome-title\">Блог про разработку и IT</h1>
        <div class=\"";
        // line 11
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__welcome-description\">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur distinctio ipsum quae quaerat
            suscipit! Assumenda eligendi, expedita laudantium molestias reprehenderit sequi? Architecto aspernatur
            mollitia quia ratione saepe soluta tempore voluptas?</p>
        </div>
        <div class=\"";
        // line 16
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__search\">
          <input type=\"text\" autocomplete=\"off\" class=\"";
        // line 17
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__search-input\" placeholder=\"Поиск по материалам сайта\">
          <button class=\"";
        // line 18
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__search-submit\">Поиск</button>
        </div>
      </div>
    </div>
  </div>
  <div class=\"";
        // line 23
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__author\">
    <img src=\"";
        // line 24
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\twig_tweak\TwigExtension')->imageStyle($this->sandbox->ensureToStringAllowed(($context["author_bg"] ?? null)), "dlog_hero"), "html", null, true);
        echo "\" alt=\"\" class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__author_bg\">
    <div class=\"";
        // line 25
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__container\">
      <div class=\"";
        // line 26
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__author-inner\">
       <img src=\"";
        // line 27
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\twig_tweak\TwigExtension')->imageStyle($this->sandbox->ensureToStringAllowed(($context["author_avatar"] ?? null)), "medium"), "html", null, true);
        echo "\" alt=\"author\" class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__author-avatar\">
        <div class=\"";
        // line 28
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__author-hello\">Привет! Меня зовут</div>
        <div class=\"";
        // line 29
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__author-name\">
          Гевин Белсон
        </div>
        <div class=\"";
        // line 32
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__author-about\">
          <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores at culpa esta rem saepe, ut vel velit? Eum, quibusdam.</span>
            <span>Aliquam at, consectetur culpa dolorem ex fugiat id iusto maiores, nisi quos tempore, vero. Blanditiis cum e soluta, tempore voluptatum.</span>
            <span>Eius placeat totam unde voluptatibus! Accusamus aliquid asperiores delectus dignio officia praesentium vel vero voluptate!</span>
            <span>Ab architecto asperiores atque blanditiis commodi cum cumque eius hic ipsa iusto laborumipit tempore temporibus veniam vero voluptate voluptatibus.</span>
            <span>Ab amet ex magnam nemo, quis sapiente. Ad labore placeat qui! Accusamustium quo recusandae suscipit unde velit.</span>
            <span>Animi assumenda beatae consequuntur dolorum ducimus et, impedit itaqu ut voluptas? Dolore, fugit vel.</span>
          </p>
        </div>
        <a href=\"";
        // line 41
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("entity.node.canonical", ["node" => 1]));
        echo "\" class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__author-more\">
          Больше об aвторе
        </a>
      </div>
    </div>
  </div>
  <div class=\"";
        // line 47
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__articles\">
    <div class=\"";
        // line 48
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__container\">
      <h2 class=\"";
        // line 49
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__articles-title\">Последние публикации</h2>
      ";
        // line 50
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, views_embed_view("blog_articles", "frontpage_articles"), "html", null, true);
        echo "
      <a href=\"";
        // line 51
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("view.blog_articles.page_1"));
        echo "\" class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__articles-more\">Перейти блог</a>
    </div>
  </div>
</main>
";
    }

    public function getTemplateName()
    {
        return "@blogger/include/pages/front-page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 51,  186 => 50,  182 => 49,  178 => 48,  174 => 47,  163 => 41,  151 => 32,  145 => 29,  141 => 28,  135 => 27,  131 => 26,  127 => 25,  121 => 24,  117 => 23,  109 => 18,  105 => 17,  101 => 16,  93 => 11,  89 => 10,  85 => 9,  81 => 8,  76 => 6,  72 => 5,  66 => 4,  62 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% set bem_block = 'front-page' %}
<main class=\"{{ bem_block }}\">
  <div class=\"{{ bem_block }}__welcome\">
    <video poster=\"{{ search_image_uri|image_style('thumbnail') }}\" autoplay loop muted class=\"{{ bem_block }}__welcome-video\">
        <source src = \"{{ file_url(search_video_uri )}}\" type=\"video/mp4\">
      {{ 'Your bowser does not support the video tag'|t }}
    </video>
    <div class=\"{{ bem_block }}__container\">
      <div class=\"{{ bem_block }}__welcome-inner\">
        <h1 class=\"{{ bem_block }}__welcome-title\">Блог про разработку и IT</h1>
        <div class=\"{{ bem_block }}__welcome-description\">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur distinctio ipsum quae quaerat
            suscipit! Assumenda eligendi, expedita laudantium molestias reprehenderit sequi? Architecto aspernatur
            mollitia quia ratione saepe soluta tempore voluptas?</p>
        </div>
        <div class=\"{{ bem_block }}__search\">
          <input type=\"text\" autocomplete=\"off\" class=\"{{ bem_block }}__search-input\" placeholder=\"Поиск по материалам сайта\">
          <button class=\"{{ bem_block }}__search-submit\">Поиск</button>
        </div>
      </div>
    </div>
  </div>
  <div class=\"{{ bem_block }}__author\">
    <img src=\"{{ author_bg|image_style('dlog_hero') }}\" alt=\"\" class=\"{{ bem_block}}__author_bg\">
    <div class=\"{{ bem_block }}__container\">
      <div class=\"{{ bem_block }}__author-inner\">
       <img src=\"{{ author_avatar| image_style('medium') }}\" alt=\"author\" class=\"{{ bem_block }}__author-avatar\">
        <div class=\"{{ bem_block }}__author-hello\">Привет! Меня зовут</div>
        <div class=\"{{ bem_block }}__author-name\">
          Гевин Белсон
        </div>
        <div class=\"{{ bem_block }}__author-about\">
          <p><span>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Asperiores at culpa esta rem saepe, ut vel velit? Eum, quibusdam.</span>
            <span>Aliquam at, consectetur culpa dolorem ex fugiat id iusto maiores, nisi quos tempore, vero. Blanditiis cum e soluta, tempore voluptatum.</span>
            <span>Eius placeat totam unde voluptatibus! Accusamus aliquid asperiores delectus dignio officia praesentium vel vero voluptate!</span>
            <span>Ab architecto asperiores atque blanditiis commodi cum cumque eius hic ipsa iusto laborumipit tempore temporibus veniam vero voluptate voluptatibus.</span>
            <span>Ab amet ex magnam nemo, quis sapiente. Ad labore placeat qui! Accusamustium quo recusandae suscipit unde velit.</span>
            <span>Animi assumenda beatae consequuntur dolorum ducimus et, impedit itaqu ut voluptas? Dolore, fugit vel.</span>
          </p>
        </div>
        <a href=\"{{ url('entity.node.canonical', {node : 1}) }}\" class=\"{{ bem_block }}__author-more\">
          Больше об aвторе
        </a>
      </div>
    </div>
  </div>
  <div class=\"{{ bem_block }}__articles\">
    <div class=\"{{ bem_block }}__container\">
      <h2 class=\"{{ bem_block }}__articles-title\">Последние публикации</h2>
      {{ drupal_view('blog_articles', 'frontpage_articles') }}
      <a href=\"{{ url('view.blog_articles.page_1') }}\" class=\"{{ bem_block }}__articles-more\">Перейти блог</a>
    </div>
  </div>
</main>
", "@blogger/include/pages/front-page.html.twig", "/var/www/web/themes/custom/blogger/templates/include/pages/front-page.html.twig");
    }
}
