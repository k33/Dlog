<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* __string_template__8e475e8adb0f8522639fe78febdbe471a4be066b9d2b9b7ab8f87ef53052c67a */
class __TwigTemplate_3084a7e1f323c04badd979114c99a2df0ef8a72796f6fba477f6311761c6c754 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1];
        $filters = ["escape" => 2, "date" => 6];
        $functions = ["snippet" => 4];

        try {
            $this->sandbox->checkSecurity(
                ['set'],
                ['escape', 'date'],
                ['snippet']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["bem_block"] = "footer";
        // line 2
        echo "<footer class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "\"><div class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__content\">
     <div class=\"";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__copyright\">
       ";
        // line 4
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('snippet')->getCallable(), ["social_links", ["bem_block" => "footer-social"]]), "html", null, true);
        echo "
    \t";
        // line 5
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_menu"] ?? null)), "html", null, true);
        echo "
       © ";
        // line 6
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " <strong>Dlog Hero</strong>. Все права защищени.
      </div> 
   </div>
</footer>";
    }

    public function getTemplateName()
    {
        return "__string_template__8e475e8adb0f8522639fe78febdbe471a4be066b9d2b9b7ab8f87ef53052c67a";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 6,  72 => 5,  68 => 4,  64 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# inline_template_start #}{% set bem_block = \"footer\" %}
<footer class=\"{{bem_block}}\"><div class=\"{{bem_block}}__content\">
     <div class=\"{{bem_block}}__copyright\">
       {{ snippet('social_links', {bem_block: 'footer-social'}) }}
    \t{{footer_menu}}
       © {{'now'|date('Y')}} <strong>Dlog Hero</strong>. Все права защищени.
      </div> 
   </div>
</footer>", "__string_template__8e475e8adb0f8522639fe78febdbe471a4be066b9d2b9b7ab8f87ef53052c67a", "");
    }
}
