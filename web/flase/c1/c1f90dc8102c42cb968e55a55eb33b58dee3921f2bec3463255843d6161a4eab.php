<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @blogger/include/pages/contact.html.twig */
class __TwigTemplate_b056b8874e06f9eb23f5e5c0a959f84f90bad99f8acbe75ed8ccffdaacdf6589 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1];
        $filters = ["escape" => 2, "image_style" => 37, "t" => 39];
        $functions = ["dlog_availability_status" => 27, "contact_form_ajax" => 33, "file_url" => 38];

        try {
            $this->sandbox->checkSecurity(
                ['set'],
                ['escape', 'image_style', 't'],
                ['dlog_availability_status', 'contact_form_ajax', 'file_url']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["bem_block"] = "contact-page";
        // line 2
        echo "<div class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "\">
  <div class=\"";
        // line 3
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__inner\">
    <div class=\"";
        // line 4
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__left\">
      <h1 class=\"";
        // line 5
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__title\">Контакти</h1>
      <div class=\"";
        // line 6
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-information\">
        <div class=\"";
        // line 7
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__lets-talk\">Давайте <span>пообщаемся</span></div>
        <div class=\"";
        // line 8
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-items\">
          <div class=\"";
        // line 9
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item\">
            <span class=\"";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item-icon ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item-icon--address\"></span>
            <div class=\"";
        // line 11
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item-label\">Адрес</div>
            <div class=\"";
        // line 12
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item-value\">Россияб Москва</div>
          </div>
          <div class=\"";
        // line 14
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item\">
            <span class=\"";
        // line 15
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item-icon ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item-icon--email\"></span>
            <div class=\"";
        // line 16
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item-label\">Email</div>
            <div class=\"";
        // line 17
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item-value\"><a href=\"mailto:gavin.belson@example.com\">gavin.belson@example.com</a></div>
          </div>
          <div class=\"";
        // line 19
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item\">
            <span class=\"";
        // line 20
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item-icon ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item-icon--phone\"></span>
            <div class=\"";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item-label\">Телефон</div>
            <div class=\"";
        // line 22
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item-value\"><a href=\"tel:+78005553535\">+7 (800) 555-35-35</a></div>
          </div>
          <div class=\"";
        // line 24
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item\">
            <span class=\"";
        // line 25
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item-icon ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item-icon--availability\"></span>
            <div class=\"";
        // line 26
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item-label\">Доступность</div>
            <div class=\"";
        // line 27
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-item-value\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('dlog_availability_status')->getCallable(), []), "html", null, true);
        echo "</div>
          </div>
        </div>
      </div>
      <div class=\"";
        // line 31
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-form\">
        <div class=\"";
        // line 32
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-form-title\">Форма <span>связи</span></div>
      ";
        // line 33
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\contact_tools\Twig\Extension\Extensions')->contactFormAjax("feedback"), "html", null, true);
        echo "
      </div>
    </div>
    <div class=\"";
        // line 36
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__right\">
      <video poster=\"";
        // line 37
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\twig_tweak\TwigExtension')->imageStyle($this->sandbox->ensureToStringAllowed(($context["contact_image_uri"] ?? null)), "thumbnail"), "html", null, true);
        echo "\" autoplay loop muted class=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__contact-video\">
        <source src = \"";
        // line 38
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, call_user_func_array($this->env->getFunction('file_url')->getCallable(), [$this->sandbox->ensureToStringAllowed(($context["contact_video_uri"] ?? null))]), "html", null, true);
        echo "\" type=\"video/mp4\">
        ";
        // line 39
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Your bowser does not support the video tag"));
        echo "
      </video>
    </div>
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@blogger/include/pages/contact.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 39,  190 => 38,  184 => 37,  180 => 36,  174 => 33,  170 => 32,  166 => 31,  157 => 27,  153 => 26,  147 => 25,  143 => 24,  138 => 22,  134 => 21,  128 => 20,  124 => 19,  119 => 17,  115 => 16,  109 => 15,  105 => 14,  100 => 12,  96 => 11,  90 => 10,  86 => 9,  82 => 8,  78 => 7,  74 => 6,  70 => 5,  66 => 4,  62 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% set bem_block = 'contact-page' %}
<div class=\"{{ bem_block }}\">
  <div class=\"{{ bem_block }}__inner\">
    <div class=\"{{ bem_block }}__left\">
      <h1 class=\"{{ bem_block }}__title\">Контакти</h1>
      <div class=\"{{ bem_block }}__contact-information\">
        <div class=\"{{ bem_block }}__lets-talk\">Давайте <span>пообщаемся</span></div>
        <div class=\"{{ bem_block }}__contact-items\">
          <div class=\"{{ bem_block }}__contact-item\">
            <span class=\"{{ bem_block }}__contact-item-icon {{ bem_block }}__contact-item-icon--address\"></span>
            <div class=\"{{ bem_block }}__contact-item-label\">Адрес</div>
            <div class=\"{{ bem_block }}__contact-item-value\">Россияб Москва</div>
          </div>
          <div class=\"{{ bem_block }}__contact-item\">
            <span class=\"{{ bem_block }}__contact-item-icon {{ bem_block }}__contact-item-icon--email\"></span>
            <div class=\"{{ bem_block }}__contact-item-label\">Email</div>
            <div class=\"{{ bem_block }}__contact-item-value\"><a href=\"mailto:gavin.belson@example.com\">gavin.belson@example.com</a></div>
          </div>
          <div class=\"{{ bem_block }}__contact-item\">
            <span class=\"{{ bem_block }}__contact-item-icon {{ bem_block }}__contact-item-icon--phone\"></span>
            <div class=\"{{ bem_block }}__contact-item-label\">Телефон</div>
            <div class=\"{{ bem_block }}__contact-item-value\"><a href=\"tel:+78005553535\">+7 (800) 555-35-35</a></div>
          </div>
          <div class=\"{{ bem_block }}__contact-item\">
            <span class=\"{{ bem_block }}__contact-item-icon {{ bem_block }}__contact-item-icon--availability\"></span>
            <div class=\"{{ bem_block }}__contact-item-label\">Доступность</div>
            <div class=\"{{ bem_block }}__contact-item-value\">{{ dlog_availability_status() }}</div>
          </div>
        </div>
      </div>
      <div class=\"{{ bem_block }}__contact-form\">
        <div class=\"{{ bem_block }}__contact-form-title\">Форма <span>связи</span></div>
      {{ contact_form_ajax('feedback') }}
      </div>
    </div>
    <div class=\"{{ bem_block }}__right\">
      <video poster=\"{{ contact_image_uri|image_style('thumbnail') }}\" autoplay loop muted class=\"{{ bem_block }}__contact-video\">
        <source src = \"{{ file_url(contact_video_uri)}}\" type=\"video/mp4\">
        {{ 'Your bowser does not support the video tag'|t }}
      </video>
    </div>
  </div>
</div>
", "@blogger/include/pages/contact.html.twig", "/var/www/web/themes/custom/blogger/templates/include/pages/contact.html.twig");
    }
}
