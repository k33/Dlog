<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @blogger/include/header.html.twig */
class __TwigTemplate_7211f032f969feee0027a495e4881a15981648a71534f7bd300ce1467b4a24a7 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1, "do" => 2, "if" => 13, "for" => 14];
        $filters = ["merge" => 15, "escape" => 18];
        $functions = ["create_attribute" => 1, "drupal_block" => 20];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'do', 'if', 'for'],
                ['merge', 'escape'],
                ['create_attribute', 'drupal_block']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["header_attributes"] = $this->env->getExtension('Drupal\Core\Template\TwigExtension')->createAttribute();
        // line 2
        $this->getAttribute(($context["header_attributes"] ?? null), "setAttribute", [0 => "role", 1 => "banner"], "method");
        // line 3
        $context["bem_block"] = "header";
        // line 4
        $context["classes"] = [0 =>         // line 5
($context["bem_block"] ?? null)];
        // line 7
        $context["brending_settings_config"] = ["use_site_logo" => false, "use_site_slogan" => false];
        // line 12
        $context["show_dlog_hero"] = (((isset($context["show_dlog_hero"]) || array_key_exists("show_dlog_hero", $context))) ? (($context["show_dlog_hero"] ?? null)) : (true));
        // line 13
        if ((isset($context["bem_modifires"]) || array_key_exists("bem_modifires", $context))) {
            // line 14
            echo "  ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["bem_modifires"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["modifer"]) {
                // line 15
                echo "    ";
                $context["classes"] = twig_array_merge($this->sandbox->ensureToStringAllowed(($context["classes"] ?? null)), [0 => (($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "--") . $this->sandbox->ensureToStringAllowed($context["modifer"]))]);
                // line 16
                echo "  ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['modifer'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        // line 18
        echo "<header ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["header_attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  <div class=\"";
        // line 19
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__top\" >
    ";
        // line 20
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\twig_tweak\TwigExtension')->drupalBlock("system_branding_block", $this->sandbox->ensureToStringAllowed(($context["brending_settings_config"] ?? null))), "html", null, true);
        echo "
    ";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\twig_tweak\TwigExtension')->drupalBlock("system_menu_block:main", ["label_display" => false]), "html", null, true);
        echo "
  </div>
  ";
        // line 23
        if (($context["show_dlog_hero"] ?? null)) {
            // line 24
            echo "    ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\twig_tweak\TwigExtension')->drupalBlock("dlog_hero"), "html", null, true);
            echo "
  ";
        }
        // line 26
        echo "</header>
";
    }

    public function getTemplateName()
    {
        return "@blogger/include/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 26,  105 => 24,  103 => 23,  98 => 21,  94 => 20,  90 => 19,  85 => 18,  78 => 16,  75 => 15,  70 => 14,  68 => 13,  66 => 12,  64 => 7,  62 => 5,  61 => 4,  59 => 3,  57 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% set header_attributes  = create_attribute()%}
{% do header_attributes.setAttribute('role', 'banner') %}
{% set bem_block = 'header' %}
{% set classes = [
  bem_block
] %}
{% set brending_settings_config =  {
    use_site_logo: false,
    use_site_slogan: false,
  }
%}
{% set show_dlog_hero = show_dlog_hero is defined ? show_dlog_hero : true %}
{% if bem_modifires is defined %}
  {% for modifer in bem_modifires %}
    {% set classes = classes|merge([bem_block ~ '--' ~ modifer]) %}
  {% endfor %}
{% endif %}
<header {{ header_attributes.addClass(classes) }}>
  <div class=\"{{ bem_block }}__top\" >
    {{ drupal_block('system_branding_block', brending_settings_config) }}
    {{ drupal_block('system_menu_block:main', {label_display: false}) }}
  </div>
  {%  if show_dlog_hero %}
    {{ drupal_block('dlog_hero') }}
  {% endif %}
</header>
", "@blogger/include/header.html.twig", "/var/www/web/themes/custom/blogger/templates/include/header.html.twig");
    }
}
