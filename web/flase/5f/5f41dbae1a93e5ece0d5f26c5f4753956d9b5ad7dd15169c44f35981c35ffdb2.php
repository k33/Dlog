<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/custom/blogger/templates/field/field--paragraph--field-title.html.twig */
class __TwigTemplate_223a41e5b7fb1d497a7f403c1d8bda19c24e53c63a164516764fab0adafbf5b8 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 2];
        $filters = ["clean_class" => 2, "replace" => 2, "escape" => 10];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set'],
                ['clean_class', 'replace', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 2
        $context["bem_block"] = ((\Drupal\Component\Utility\Html::getClass(((($this->sandbox->ensureToStringAllowed(($context["entity_type"] ?? null)) . "-") . $this->sandbox->ensureToStringAllowed(($context["bundle"] ?? null))) . (((($context["view_mode"] ?? null) != "default")) ? (("-" . $this->sandbox->ensureToStringAllowed(($context["view_mode"] ?? null)))) : ("")))) . "__") . \Drupal\Component\Utility\Html::getClass(twig_replace_filter($this->sandbox->ensureToStringAllowed(($context["field_name"] ?? null)), [($this->sandbox->ensureToStringAllowed(($context["bundle"] ?? null)) . "__") => ""])));
        // line 3
        $context["bem_element_prefix"] = ((($context["bem_element_prefix"] ?? null)) ? (($context["bem_element_prefix"] ?? null)) : (($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "-")));
        // line 4
        $context["classes"] = ((($context["classes"] ?? null)) ? (($context["classes"] ?? null)) : ([0 =>         // line 5
($context["bem_block"] ?? null), 1 => "paragraph-title"]));
        // line 8
        $context["paragraph"] = $this->getAttribute(($context["element"] ?? null), "#object", [], "array");
        // line 9
        $context["title_wrapper"] = $this->getAttribute(($context["paragraph"] ?? null), "getBehaviorSetting", [0 => "dlog_paragraphs_paragraph_title", 1 => "title_wrapper", 2 => "h2"], "method");
        // line 10
        echo "<";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_wrapper"] ?? null)), "html", null, true);
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
   ";
        // line 11
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute($this->getAttribute(($context["items"] ?? null), 0, [], "array"), "content", [])), "html", null, true);
        echo "
</";
        // line 12
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_wrapper"] ?? null)), "html", null, true);
        echo ">



";
    }

    public function getTemplateName()
    {
        return "themes/custom/blogger/templates/field/field--paragraph--field-title.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 12,  72 => 11,  66 => 10,  64 => 9,  62 => 8,  60 => 5,  59 => 4,  57 => 3,  55 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{# ENTITY_TYPE-ENTITY_BUNDLE-VIEW_MODE-FIELD_NAME #}
{% set bem_block = (entity_type ~ '-' ~ bundle ~ (view_mode != 'default' ? '-' ~ view_mode))|clean_class ~ '__' ~ field_name|replace({(bundle ~ '__'): ''})|clean_class %}
{% set bem_element_prefix = bem_element_prefix ?: bem_block ~ '-' %}
{% set classes = classes ?: [
  bem_block,
  'paragraph-title',
] %}
{% set paragraph = element['#object'] %}
{% set title_wrapper = paragraph.getBehaviorSetting('dlog_paragraphs_paragraph_title', 'title_wrapper', 'h2') %}
<{{ title_wrapper }}{{ attributes.addClass(classes) }}>
   {{ items[0].content }}
</{{ title_wrapper }}>



", "themes/custom/blogger/templates/field/field--paragraph--field-title.html.twig", "/var/www/web/themes/custom/blogger/templates/field/field--paragraph--field-title.html.twig");
    }
}
