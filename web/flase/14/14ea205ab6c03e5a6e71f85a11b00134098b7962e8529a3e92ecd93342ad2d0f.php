<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/glisseo/templates/content/comment/comment.html.twig */
class __TwigTemplate_ae1b74041dce063a9b65daf7cc4d37c62b824a2e96da0fb22b25b5db70fb1094 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'title' => [$this, 'block_title'],
            'footer' => [$this, 'block_footer'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["set" => 1, "block" => 15, "if" => 16];
        $filters = ["clean_class" => 1, "escape" => 6];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block', 'if'],
                ['clean_class', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $context["bem_block"] = ((($context["bem_block"] ?? null)) ? (($context["bem_block"] ?? null)) : (\Drupal\Component\Utility\Html::getClass((("comment-" . $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["comment"] ?? null), "bundle", []))) . ((($this->getAttribute(($context["elements"] ?? null), "#view_mode", [], "array") != "default")) ? (("-" . $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["elements"] ?? null), "#view_mode", [], "array")))) : (""))))));
        // line 2
        $context["classes"] = [0 =>         // line 3
($context["bem_block"] ?? null), 1 => (($this->sandbox->ensureToStringAllowed(        // line 4
($context["bem_block"] ?? null)) . "--") . $this->sandbox->ensureToStringAllowed(($context["status"] ?? null)))];
        // line 6
        echo "<article";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["attributes"] ?? null), "addClass", [0 => "js-comment", 1 => ($context["classes"] ?? null)], "method")), "html", null, true);
        echo ">
  ";
        // line 12
        echo "  <mark class=\"hidden\" data-comment-timestamp=\"";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["new_indicator_timestamp"] ?? null)), "html", null, true);
        echo "\"></mark>

  <div";
        // line 14
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content_attributes"] ?? null), "addClass", [0 => ($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "__content")], "method")), "html", null, true);
        echo ">
    ";
        // line 15
        $this->displayBlock('content', $context, $blocks);
        // line 28
        echo "  </div>

  <footer class=\"";
        // line 30
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)), "html", null, true);
        echo "__footer\">
    ";
        // line 31
        $this->displayBlock('footer', $context, $blocks);
        // line 46
        echo "  </footer>
</article>
";
    }

    // line 15
    public function block_content($context, array $blocks = [])
    {
        // line 16
        echo "      ";
        if (($context["title"] ?? null)) {
            // line 17
            echo "        ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
            echo "
        <h3";
            // line 18
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["title_attributes"] ?? null), "addClass", [0 => ($this->sandbox->ensureToStringAllowed(($context["bem_block"] ?? null)) . "__title")], "method")), "html", null, true);
            echo ">
          ";
            // line 19
            $this->displayBlock('title', $context, $blocks);
            // line 22
            echo "        </h3>
        ";
            // line 23
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
            echo "
      ";
        }
        // line 25
        echo "
      ";
        // line 26
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content"] ?? null)), "html", null, true);
        echo "
    ";
    }

    // line 19
    public function block_title($context, array $blocks = [])
    {
        // line 20
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null)), "html", null, true);
        echo "
          ";
    }

    // line 31
    public function block_footer($context, array $blocks = [])
    {
        // line 32
        echo "      ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["user_picture"] ?? null)), "html", null, true);
        echo "
      <p>";
        // line 33
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["submitted"] ?? null)), "html", null, true);
        echo "</p>

      ";
        // line 40
        echo "      ";
        if (($context["parent"] ?? null)) {
            // line 41
            echo "        <p class=\"visually-hidden\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["parent"] ?? null)), "html", null, true);
            echo "</p>
      ";
        }
        // line 43
        echo "
      ";
        // line 44
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["permalink"] ?? null)), "html", null, true);
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "themes/contrib/glisseo/templates/content/comment/comment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 44,  163 => 43,  157 => 41,  154 => 40,  149 => 33,  144 => 32,  141 => 31,  134 => 20,  131 => 19,  125 => 26,  122 => 25,  117 => 23,  114 => 22,  112 => 19,  108 => 18,  103 => 17,  100 => 16,  97 => 15,  91 => 46,  89 => 31,  85 => 30,  81 => 28,  79 => 15,  75 => 14,  69 => 12,  64 => 6,  62 => 4,  61 => 3,  60 => 2,  58 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("{% set bem_block = bem_block ?: ('comment-' ~ comment.bundle ~ (elements['#view_mode'] != 'default' ? '-' ~ elements['#view_mode']))|clean_class %}
{% set classes = [
  bem_block,
  bem_block ~ '--' ~ status,
] %}
<article{{ attributes.addClass('js-comment', classes) }}>
  {#
    Hide the \"new\" indicator by default, let a piece of JavaScript ask the
    server which comments are new for the user. Rendering the final \"new\"
    indicator here would break the render cache.
  #}
  <mark class=\"hidden\" data-comment-timestamp=\"{{ new_indicator_timestamp }}\"></mark>

  <div{{ content_attributes.addClass(bem_block ~ '__content') }}>
    {% block content %}
      {% if title %}
        {{ title_prefix }}
        <h3{{ title_attributes.addClass(bem_block ~ '__title') }}>
          {% block title %}
            {{ title }}
          {% endblock %}
        </h3>
        {{ title_suffix }}
      {% endif %}

      {{ content }}
    {% endblock %}
  </div>

  <footer class=\"{{ bem_block }}__footer\">
    {% block footer %}
      {{ user_picture }}
      <p>{{ submitted }}</p>

      {#
        Indicate the semantic relationship between parent and child comments for
        accessibility. The list is difficult to navigate in a screen reader
        without this information.
      #}
      {% if parent %}
        <p class=\"visually-hidden\">{{ parent }}</p>
      {% endif %}

      {{ permalink }}
    {% endblock %}
  </footer>
</article>
", "themes/contrib/glisseo/templates/content/comment/comment.html.twig", "/var/www/web/themes/contrib/glisseo/templates/content/comment/comment.html.twig");
    }
}
