<?php

namespace Drupal\dlog_paragraphs\Plugin\paragraphs\Behavior;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use \Drupal\paragraphs\ParagraphsBehaviorBase;


/**
 * @ParagraphsBehavior(
 *   id = "dlog_paragraphs_paragraph_remote_video_behavior",
 *   label = @Translation("Remote video settigns"),
 *   description = @Translation("Allows cofigurate settings for remote video paragraph"),
 *   weight = 0,
 * )
 */
class RemoteVideoBehavior extends ParagraphsBehaviorBase {

  /**
   * @inheritdoc
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
    $bem_block = 'paragraph-' . $paragraph->bundle() . ($view_mode == 'default' ? '' : '-' . $view_mode);
    $maximum_width_video = $paragraph->getBehaviorSetting($this->getPluginId(), 'video_width', 'full');
    $build['#attributes']['class'][] = Html::getClass($bem_block . '--video-width-' . $maximum_width_video);

  }

  /**
   * @inheritdoc
   */
  public  static function isApplicable(ParagraphsType $paragraphs_type) {
    return $paragraphs_type->id() == 'remote_video';
  }

  /**
   * @inheritdoc
   */
  public function  buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {

    $form['video_width'] = [
      '#type' => 'select',
      '#title' => $this->t('Video width'),
      '#description' => 'Maximum width for video',
      '#options' => [
        'full' => '100%',
        '720px' => '720px',
      ],
      '#default_options' => $paragraph->getBehaviorSetting($this->getPluginId(), 'video_width', 'full'),
    ];

    return $form;
  }
}
