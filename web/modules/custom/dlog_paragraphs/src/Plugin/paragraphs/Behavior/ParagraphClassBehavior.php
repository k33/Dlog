<?php

namespace Drupal\dlog_paragraphs\Plugin\paragraphs\Behavior;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use \Drupal\paragraphs\ParagraphsBehaviorBase;
use  \Drupal\Component\Utility\Html;

/**
 * @ParagraphsBehavior(
 *   id = "dlog_paragraphs_calass",
 *   label = @Translation("Custm classes for paragraphs."),
 *   description = @Translation("Allows add custom classes to paragraphs."),
 *   weight = 0,
 * )
 */
class ParagraphClassBehavior extends ParagraphsBehaviorBase {

  /**
   * @inheritdoc
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
    $classes_value = $paragraph->getBehaviorSetting($this->getPluginId() , 'classes', '');
    $classes  = explode(' ', $classes_value );
    $bem_block = 'paragraph-' . $paragraph->bundle() . ($view_mode == 'default' ? '' : '-' . $view_mode);
    foreach ($classes as $class) {
      $build['#attributes']['class'][] =  $bem_block  . '--' . Html::getClass($class);
    }
  }

  /**
   * @inheritdoc
   */
  public  static function isApplicable(ParagraphsType $paragraphs_type) {
    return TRUE;
  }

  /**
   * @inheritdoc
   */
  public function  buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $form['classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Classes'),
      '#description' => $this->t('Multiple classes separated by space. They will be processed via Html:getClass()'),
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId() , 'classes', ''),
    ];
  }
}
