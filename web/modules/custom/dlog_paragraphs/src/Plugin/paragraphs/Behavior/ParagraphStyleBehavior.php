<?php

namespace Drupal\dlog_paragraphs\Plugin\paragraphs\Behavior;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use \Drupal\paragraphs\ParagraphsBehaviorBase;
use  \Drupal\Component\Utility\Html;

/**
 * @ParagraphsBehavior(
 *   id = "dlog_paragraphs_paragraph_style",
 *   label = @Translation("Pragraphs style"),
 *   description = @Translation("Allows add custom style to paragraphs."),
 *   weight = 0,
 * )
 */
class ParagraphStyleBehavior extends ParagraphsBehaviorBase {

  /**
   * @inheritdoc
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
    $selected_styles = $paragraph->getBehaviorSetting($this->getPluginId(), 'styles', []);
    $bem_block = 'paragraph-style';
    foreach ($selected_styles as $style) {
      $build['#attributes']['class'][] = $bem_block . '--' . Html::getClass($style);
    }

  }

  /**
   * @inheritdoc
   */
  public  static function isApplicable(ParagraphsType $paragraphs_type) {
    return TRUE;
  }

  /**
   * @inheritdoc
   */
  public function  buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {

    $form['style_wrapper'] = [
      '#type' => 'details',
      '#title' => $this->t('Paragraph style'),
      '#open' => FALSE,
    ];

    $styles = $this->getStyle($paragraph);
    $selected_styles = $paragraph->getBehaviorSetting($this->getPluginId(), 'styles', []);

    foreach ($styles as $group_id => $group) {
      $form['style_wrapper'][$group_id] = [
        '#type' => 'checkboxes',
        '#title' => $group['label'],
        '#options' => $group['options'],
        '#default_value' => $selected_styles,
      ];
    }

    return $form;
  }

  public  function submitBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {
    $styles = [];
    $filtered_values = $this->filterBehaviorFormSubmitValues($paragraph,  $form, $form_state);

   if (!empty($filtered_values)) {
     $styles_groups = $filtered_values['style_wrapper'];

     foreach ($styles_groups as $group) {
       foreach ($group as $style_name) {
         $styles[] = $style_name;
       }
     }

   }

    $paragraph->setBehaviorSettings($this->getPluginId(), ['styles' => $styles]);
  }

  /**
   * Return style for paragraph.
   */
  public  function getStyle(ParagraphInterface $paragraph) {
    $styles = [];

    if ($paragraph->hasField('field_title')) {
      $styles['title'] = [
        'label' => $this->t('Paragraphs title'),
        'options' => [
          'title_bold' => $this->t('Bold'),
          'title_centered' => $this->t('Center'),
        ]
      ];
    }

    $styles['common'] = [
      'label' => $this->t('Paragraphs common style'),
      'options' => [
        'style_black' => $this->t('Style black')

      ]
    ];

    return $styles;
  }


}
