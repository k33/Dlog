<?php

namespace Drupal\dlog_paragraphs\Plugin\paragraphs\Behavior;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs\Entity\ParagraphsType;
use Drupal\paragraphs\ParagraphInterface;
use \Drupal\paragraphs\ParagraphsBehaviorBase;


/**
 * @ParagraphsBehavior(
 *   id = "dlog_paragraphs_paragraph_remote_video_gallery_behavior",
 *   label = @Translation("Remote video gallery settigns"),
 *   description = @Translation("Allows cofigurate settings for remote video paragraph"),
 *   weight = 0,
 * )
 */
class RemoteVideoGalleryBehavior extends ParagraphsBehaviorBase {

  /**
   * @inheritdoc
   */
  public function view(array &$build, Paragraph $paragraph, EntityViewDisplayInterface $display, $view_mode) {
    $images_rep_row = $paragraph->getBehaviorSetting($this->getPluginId(), 'items_pre_row', 4);
    $bem_block = 'paragraph-' . $paragraph->bundle() . ($view_mode == 'default' ? '' : '-' . $view_mode). '--videos-pre-row-' . $images_rep_row;
    $build['#attributes']['class'][] = Html::getClass($bem_block);

    /**@todo Image style*/
  }

  /**
   * @inheritdoc
   */
  public  static function isApplicable(ParagraphsType $paragraphs_type) {

    if ($paragraphs_type->id() == 'gallery_remote_video') {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * @inheritdoc
   */
  public function  buildBehaviorForm(ParagraphInterface $paragraph, array &$form, FormStateInterface $form_state) {

    $form['items_pre_row'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of videos per row'),
      '#options' => [
        '2' => $this->formatPlural(2, '1 video per row', '@count videos per row'),
        '3' => $this->formatPlural(3, '1 video per row', '@count videos per row'),
        '4' => $this->formatPlural(4, '1 video per row', '@count videos per row'),
      ],
      '#default_value' => $paragraph->getBehaviorSetting($this->getPluginId() , 'items_pre_row', 4),
    ];

    return $form;

  }

}
