<?php

namespace Drupal\dlog_taxonomy\Plugin\DlogHero\Path;

use Drupal\dlog_hero\Plugin\DlogHero\Path\DlogHeroPathPluginBase;
use Drupal\media\MediaInterface;

/**
 * Hero block for path.
 *
 * @DlogHeroPath(
 *  id = "dlog_tag_page",
 *  match_type = "listed",
 *  match_path = {"/tags"},
 * )
 *
 */
class  DlogTagPage extends DlogHeroPathPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeroImage() {
    $media_storage = $this->getEntityTypeManager()->getStorage('media');
    $media_entity = $media_storage->load(11);

    if ($media_entity instanceof MediaInterface) {
      return $media_entity->get('field_media_image')->entity->getFileUri();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getHeroVideo() {
    $media_storage = $this->getEntityTypeManager()->getStorage('media');
    $media_entity = $media_storage->load(22);

    if ($media_entity instanceof MediaInterface) {
      return [
        'video/mp4' => $media_entity->get('field_media_video_file')->entity->getFileUri(),
      ];
    }
  }
}
