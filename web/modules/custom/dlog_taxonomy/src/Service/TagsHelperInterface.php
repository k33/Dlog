<?php

namespace Drupal\dlog_taxonomy\Service;

/**
 * Class with helper for taxonomy vocabulary tags.
 *
 * @package Drupal\dlog_taxonomy\Service
 */
interface TagsHelperInterface {

  /**
   * Gets promo image uri form taxonomy term.
   * @param int $tid
   *  The taxonomy term id.
   * @return string
   *  The file URI, NULL otherwise.
   */
  public function getPromoUri($tid);
}
