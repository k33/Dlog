<?php

namespace Drupal\dlog_taxonomy\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;
use Drupal\node\NodeInterface;
use Drupal\node\NodeStorage;
use Drupal\taxonomy\TermInterface;
use Drupal\taxonomy\TermStorage;
use Drupal\taxonomy\TermStorageInterface;

/**
 * Class with helper for taxonomy vocabulary tags.
 *
 * @package Drupal\dlog_taxonomy\Service
 */
class TagsHelper implements TagsHelperInterface {

  /** The rem storage
   * @var \Drupal\taxonomy\TermStorageInterface
   */
  protected $term_storage;
  /**
   *  The node storage.
   * @var \Drupal\node\NodeStorageInterface
   */
  protected $node_storage;

  /**
   * TagsHelper constructor.
   *
   * @param EntityTypeManagerInterface $entity_type_manager
   *  The entity type manager.
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->term_storage = $entity_type_manager->getStorage('taxonomy_term');
    $this->node_storage = $entity_type_manager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   */
  public function getPromoUri($tid) {
    /** @var \Drupal\taxonomy\TermInterface $term */
    $term = $this->term_storage->load($tid);

    if ($term instanceof TermInterface && $term->bundle() == 'tags') {
      if ($term->get('field_promo_image')->isEmpty()) {
        $last_blog_article = $this->node_storage->getQuery()
          ->condition('status', NodeInterface::PUBLISHED)
          ->condition('type', 'blog_article')
          ->condition('field_tags', $tid,'IN')
          ->sort('created', 'desc')
          ->range(0, 1)
          ->execute();

        if (!empty($last_blog_article)) {
          /** @var \Drupal\node\NodeInterface $last_blog_article */
          $last_blog_article = $this->node_storage
            ->load(array_shift($last_blog_article));

          if ($last_blog_article->hasField('field_promo_image') && !$last_blog_article->get('field_promo_image')->isEmpty()) {

            /** @var \Drupal\media\MediaInterface $media */
            $media = $last_blog_article->get('field_promo_image')->entity;
            /** @var \Drupal\File\FileInterface $file */
            $file = $media->get('field_media_image')->entity;

            return $file->getFileUri();
          }
        }
      }
      else {
        /** @var \Drupal\media\MediaInterface $media */
        $media = $term->get('field_promo_image')->entity;
        /** @var \Drupal\File\FileInterface $file */
        $file = $media->get('field_media_image')->entity;

        return $file->getFileUri();
      }
    }
    return NULL;
  }
}
