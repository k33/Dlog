<?php

namespace  Drupal\dlog_hero\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * DlogHeroPath annotation.
 *
 * @Annotation
 */
class DlogHeroPath extends Plugin {

  /**
   * The plugin ID.
   * @var string
   */
  public $id;

  /**
   * The plugin status.
   *
   * By default all plugins are enabled adn this value set in TRUE. You can set
   * it to FALSE, to temporary disable plugin.
   *
   * @var bool
   */
  public $enabled;

  /**
   * The paths to match.
   *
   * An array with paths to limit plugin execution. Can contain wildcard.
   * (*) and Drupal placeholder such as <front>.
   *
   * @var array
   */
  public $match_path;

  /**
   * Then match type for match_path.
   *
   * Value can be:
   * - listed: (default) Shows only at path from match_path.
   * - unlisted: Show at all paths, except those listed in match_path.
   * @var string
   */
  public $match_type;

  /**
   * The weight of plugin.
   *
   * Plugin with higher with, will be used.
   *
   * @var int
   */
  public $weight;

}
