<?php


namespace Drupal\dlog_hero\Plugin\DlogHero;

use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Controller\TitleResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * The base for all DlogHero plugins.
 */
abstract class DlogHeroPluginBase extends PluginBase implements DlgoHeroPluginInterface, ContainerFactoryPluginInterface {
  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch
   */
  protected $current_route_match;
  /**
   * The current page title.
   *
   * @var array|string|null
   */
  protected $page_title;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entity_type_manager;

  /**
   * DlogHeroPluginBase constructor.
   *
   * @param array $configuration
   *  The configuration.
   * @param $plugin_id
   *  The plugin ID.
   * @param mixed $plugin_definition
   *  The plugin definition.
   * @param Request $request
   *  The current request.
   * @param CurrentRouteMatch $current_route_match
   *  The current route match.
   * @param TitleResolverInterface $title_resolver
   *  The title resolver.
   * @param EntityTypeManagerInterface $entity_type_manager
   *  The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Request $request, CurrentRouteMatch $current_route_match, TitleResolverInterface $title_resolver, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->request = $request;
    $this->current_route_match = $current_route_match;
    $this->page_title = $title_resolver->getTitle($this->request, $this->current_route_match->getRouteObject());
    $this->entity_type_manager = $entity_type_manager;
  }

  /**
   *{@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('current_route_match'),
      $container->get('title_resolver'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Gets current request.
   *
   * @return \Symfony\Component\HttpFoundation\Request
   *  The current request.
   */
  public function getRequest () {
    return $this->request;
  }

  /**
   * Gets current route match.
   *
   * @return \Drupal\Core\Routing\CurrentRouteMatch
   *  The current route match.
   */
  public function getRouteMatch() {
    return $this->current_route_match;
  }

  /**
   * Gets current page title.
   *
   * @return array|string|null
   *  The current page title.
   */
  public function getPageTitle() {
    return $this->page_title;
  }

  /**
   * Gets entity type manger.
   *
   * @return  \Drupal\Core\Entity\EntityTypeManagerInterface
   *  The entity type manger.
   */
  public function getEntityTypeManager() {
    return $this->entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public  function getEnabled() {
   return $this->pluginDefinition['enabled'];
  }

  /**
   * {@inheritdoc}
   */
  public function getWeight() {
    return $this->pluginDefinition['weight'];
  }

  /**
   * {@inheritdoc}
   */
 public function getHeroTitle() {
   return $this->getPageTitle();
 }

  /**
   * {@inheritdoc}
   */
  public function getHeroSubtitle() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getHeroImage() {
    return NULL;
  }

  /**
   * {@inheritdoc}
  */
  public function getHeroVideo() {
    return [];
  }

}
