<?php


namespace Drupal\dlog_hero\Plugin\DlogHero\Path;


use Drupal\dlog_hero\Plugin\DlogHero\DlogHeroPluginBase;

/**
 *Default plugin which will be userd if non others  met their requirements.
 *
 * @DlogHeroPath(
 *   id = "dlog_hero_path_default",
 *   match_path = {"*"},
 *   weight = -100,
 * )
 */
class DlogHeroPathDefaultPlugin extends DlogHeroPluginBase {


}
