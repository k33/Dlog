<?php

namespace Drupal\dlog_hero\Plugin\DlogHero;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcher;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Plugin\Factory\ContainerFactory;
use Drupal\Core\Routing\CurrentRouteMatch;
use Symfony\Component\DependencyInjection\Container;

/**
 * DlogHero plugin manager.
 */
class DlogheroPluginManager extends DefaultPluginManager {

  /**
   *  The current path stack.
   *
   * @var \Drupal\Core\Path\CurrentPathStack.
   */
  protected $path_current;
  /**
   * The path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcher.
   */
  protected $path_matcher;
  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\CurrentRouteMatch.
   */
  protected $current_route_match;

  /**
   * DlogheroPluginManager constructor.
   *
   * @param $type
   *   The DlogHero plugin type.The current route match.
   * @param \Traversable $namespaces
   *   The namespaces.
   * @param CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param ModuleHandlerInterface $module_handler
   *  The module handler.
   * @param CurrentPathStack $path_current
   *  The current path stack.
   * @param PathMatcher $path_matcher
   *  The path matcher.
   * @param CurrentRouteMatch $current_route_match
   *  The current route match.
   */
  public function __construct($type, \Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler,
    CurrentPathStack $path_current, PathMatcher $path_matcher, CurrentRouteMatch $current_route_match) {

    $this->path_current = $path_current;
    $this->path_matcher = $path_matcher;
    $this->current_route_match = $current_route_match;
    $type_camelized = Container::camelize($type);
    $subdir = "Plugin/DlogHero/{$type_camelized}";
    $plugin_interface = "Drupal\dlog_hero\Plugin\DlogHero\{$type_camelized}\DlogHero{$type_camelized}PluginInterface";
    $plugin_definition_annotation_name = "Drupal\dlog_hero\Annotation\DlogHero{$type_camelized}";

    parent::__construct($subdir, $namespaces, $module_handler, $plugin_interface, $plugin_definition_annotation_name);

   $this->defaults += [
      'plugin_type' => $type,
      'enabled' => TRUE,
      'weight' => 0,
     ];

   if ($type == 'path') {
     $this->defaults += [
       'match_type' => 'listed',
     ];
   }

    //Call hook_dlog_hero_TYPE_alter().
    $this->alterInfo("dlog_hero_{$type}");

    $this->setCacheBackend($cache_backend, "dlog_hero:{$type}");
    $this->factory = new ContainerFactory($this->getDiscovery());
  }

  /**
   * Gets suitable plugins for current request.
   */
  public function getSuitablePlugins() {
    $plugin_type = $this->defaults['plugin_type'];
    if($plugin_type == 'entity') {
      return $this->getSuitableEntityPlugins();
    }

    if ($plugin_type == 'path') {
      return $this->getSuitablePathPlugins();
    }
    return [];
  }

  /**
   * Gets dlog hero entity plugin for suitable current request.
   */
  protected function getSuitableEntityPlugins() {
    $plugins = [];
    $entity = NULL;

    foreach ($this->current_route_match->getParameters() as $parameter) {
      if ($parameter instanceof EntityInterface) {
        $entity = $parameter;
        break;
      }
    }
    if ($entity) {
      foreach ($this->getDefinitions() as $plugin_id  => $plugin) {
        if ($plugin['enabled']) {
          $same_entity_type = $plugin['entity_type'];
          $needed_bundle = in_array($entity->bundle(), $plugin['entity_bundle']) || in_array('*', $plugin['entity_bundle']);

          if ($same_entity_type && $needed_bundle) {
            $plugins[$plugin_id] = $plugin;
            $plugins[$plugin_id]['entity'] = $entity;
          }
        }

      }
    }

    uasort($plugins, '\Drupal\Component\Utility\SortArray::sortByWeightElement');

    return $plugins;
  }

  /**
   * Gets dlog hero path plugin for suitable current request.
   */
  protected function getSuitablePathPlugins() {
    $plugins = [];

    foreach ($this->getDefinitions() as $plugin_id => $plugin) {
      if($plugin['enabled']) {
        $patterns = implode(PHP_EOL, $plugin['match_path']);
        $current_path = $this->path_current->getPath();
        $is_match_path  = $this->path_matcher->matchPath($current_path, $patterns);
        switch($plugin['match_type']) {
          case 'listed':
            $match_type = 0;
            break;
          case 'unlisted':
            $match_type = 1;
            break;
          default:
            $match_type = 0;
            break;
        }

        $is_plugin_needed = ($is_match_path xor $match_type);

        if ($is_plugin_needed) {
          $plugins[$plugin_id] = $plugin;
        }
      }
    }

    uasort($plugins, '\Drupal\Component\Utility\SortArray::sortByWeightElement');
    return $plugins;
  }

}
