<?php

namespace Drupal\dlog_hero\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\dlog_hero\Plugin\DlogHero\DlogheroPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a dlog hero block.
 *
 * @Block(
 *   id = "dlog_hero",
 *   admin_label = @Translation("Dlog Hero"),
 *   category = @Translation("Custom")
 * )
 */
class DlogHeroBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The plugin manager for dlog hero entity plugins.
   *
   * @var \Drupal\dlog_hero\Plugin\DlogHero\DlogheroPluginManager
   */
  protected $dlog_hero_entity_manger;


  /**
   * The plugin manager for dlog hero path plugins.
   *
   * @var \Drupal\dlog_hero\Plugin\DlogHero\DlogheroPluginManager
   */
  protected $dlog_hero_path_manger;

  /**
   * DlogHeroBlock constructor.
   * @param array $configuration
   * @param $plugin_id
   * @param $plugin_definition
   * @param DlogheroPluginManager $dlog_hero_entity
   *  The plugin manager for dlog hero entity plugins.
   * @param DlogheroPluginManager $dlog_hero_path
   * The plugin manager for dlog hero path plugins.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, DlogheroPluginManager $dlog_hero_entity,
  DlogheroPluginManager $dlog_hero_path) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->dlog_hero_entity_manger = $dlog_hero_entity;
    $this->dlog_hero_path_manger = $dlog_hero_path;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $build = [];

    $path_plugins = $this->dlog_hero_path_manger->getSuitablePlugins();
    $entity_plugins = $this->dlog_hero_entity_manger->getSuitablePlugins();

    $plugins = $entity_plugins + $path_plugins;

    uasort($plugins, '\Drupal\Component\Utility\SortArray::sortByWeightElement');

    $plugin = end($plugins);
    if ($plugin['plugin_type'] == 'entity') {
      /** @var \Drupal\dlog_hero\Plugin\DlogHero\DlgoHeroPluginInterface $instance */
      $instance = $this->dlog_hero_entity_manger->createInstance($plugin['id'], ['entity' => $plugin['entity']]);
    }

    if ($plugin['plugin_type'] == 'path' ) {
      /** @var \Drupal\dlog_hero\Plugin\DlogHero\DlgoHeroPluginInterface $instance */
      $instance = $this->dlog_hero_path_manger->createInstance($plugin['id']);
    }

    if (isset($instance)) {
      $build['content'] = [
        '#theme' => 'dlog_hero',
        '#title' => $instance->getHeroTitle(),
        '#subtitle' => $instance->getHeroSubtitle(),
        '#image' => $instance->getHeroImage(),
        '#video' => $instance->getHeroVideo(),
        '#plugin_id' => $instance->getPluginId(),
      ];
    }

    return $build;

  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static (
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.dlog_hero.entity'),
      $container->get('plugin.manager.dlog_hero.path')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return [
      'url.path'
    ];
  }

}
