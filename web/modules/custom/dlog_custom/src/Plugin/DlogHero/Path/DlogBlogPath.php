<?php

namespace Drupal\dlog_custom\Plugin\DlogHero\Path;

use \Drupal\dlog_hero\Plugin\DlogHero\Path\DlogHeroPathPluginBase;
use Drupal\media\MediaInterface;


/**
 * @DlogHeroPath(
 *  id = "dlog_hero_path",
 *  match_type = "listed",
 *  match_path = {"/blog"},
 * )
 */
class DlogBlogPath extends DlogHeroPathPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeroSubtitle() {
    return "Записи в блог";
  }

  /**
   * {@inheritdoc}
   */
  public function getHeroImage() {
    $media_storage = $this->getEntityTypeManager()->getStorage('media');
    $media_entity = $media_storage->load(11);

    if ($media_entity instanceof MediaInterface) {
      return $media_entity->get('field_media_image')->entity->getFileUri();
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getHeroVideo() {
    $media_storage = $this->getEntityTypeManager()->getStorage('media');
    $media_entity = $media_storage->load(10);

    if ($media_entity instanceof MediaInterface) {
      return [
        'video/mp4' => $media_entity->get('field_media_video_file')->entity->getFileUri(),
      ];
    }

  }

}
