<?php

namespace Drupal\dlog_custom\EventSubscriber;

use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Dlog-custom event subscriber.
 */
class DlogCustomSubscriber implements EventSubscriberInterface {

  /**
   *  The session manager.
   *
   * @var SessionManagerInterface
   */
  protected $session_manager;

  public function __construct(SessionManagerInterface $session_manager) {
    $this->session_manager = $session_manager;
  }

  /**
   * Kernel request event handler.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseEvent $event
   *   Response event.
   */
  public function onKernelRequest(GetResponseEvent $event) {
    // For #lazy_builder work for anonymous.
    $this->session_manager->start();
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => ['onKernelRequest'],
     ];
  }

}
