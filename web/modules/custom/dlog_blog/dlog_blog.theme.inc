<?php

/**
 * @file
 * Main file for custom theme hook preprocess.
 */

use Drupal\node\NodeInterface;

/**
 * Implements template preprocess_HOOK for dlog-blog-related-posts.html.twig.
 */
function template_preprocess_dlog_blog_related_posts(array &$variables) {
  $items = [];
  $node = \Drupal::routeMatch()->getParameter('node');

  /** @var Drupal\node\NodeInterface $node */
  if ($node instanceof NodeInterface) {
    /** @var  \Drupal\node\NodeStorage $node_storage */
    $node_storage = \Drupal::entityTypeManager()->getStorage('node');

    /** @var \Drupal\node\NodeViewBuilder $node_view_builder */
    $node_view_builder = \Drupal::entityTypeManager()->getViewBuilder('node');

    /** @var \Drupal\dlog_blog\Service\BlogManagerInterface  $blog_manger */
    $blog_manger = \Drupal::service('dlog_blog.manager');

    $get_related_posts_ids = $blog_manger->getRelatedPosts($node, $variables['max'], $variables['max_exact_same_tags']);

    foreach ($get_related_posts_ids as $id) {
      /** @var Drupal\node\NodeInterface $node */
      $related_post = $node_storage->load($id);
      $items[] = $node_view_builder->view($related_post, 'teaser');
    }

    $variables['items'] = $items;
  }
}

/**
 * Implements template preprocess_HOOK for dlog-blog-random-posts.html.twig.
 */
function template_preprocess_dlog_blog_random_posts(array &$variables) {
  $items = [];
  $node = \Drupal::routeMatch()->getParameter('node');

  /** @var Drupal\node\NodeInterface $node */
  if ($node instanceof NodeInterface) {
    /** @var  \Drupal\node\NodeStorage $node_storage */
    $node_storage = \Drupal::entityTypeManager()->getStorage('node');

    /** @var \Drupal\node\NodeViewBuilder $node_view_builder */
    $node_view_builder = \Drupal::entityTypeManager()->getViewBuilder('node');

    /** @var \Drupal\dlog_blog\Service\BlogManagerInterface  $blog_manger */
    $blog_manger = \Drupal::service('dlog_blog.manager');

    $get_random_posts_ids = $blog_manger->getRandomPosts(4, [$node->id()]);

    foreach ($get_random_posts_ids as $id) {
      /** @var Drupal\node\NodeInterface $node */
      $related_post = $node_storage->load($id);
      $items[] = $node_view_builder->view($related_post, 'teaser');
    }

    $variables['items'] = $items;
  }
}
