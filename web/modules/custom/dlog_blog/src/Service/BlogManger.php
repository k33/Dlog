<?php

namespace Drupal\dlog_blog\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;

/**
 *  Simple helpers for blog articles.
 *
 * @package Drupal\dlog_blog\Service
 */
class BlogManger implements BlogManagerInterface {

  /** The entity type manager
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entity_type_manager;

  /**  The node storage
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $node_storage;

  /**
   *  The node view builder.
   *
   * @var \Drupal\Core\Entity\EntityViewBuilderInterface
   */
  protected $node_view_builder;

  /**
   * BlogManger constructor.
   * @param EntityTypeManagerInterface $entity_type_manager
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entity_type_manager  = $entity_type_manager;
    $this->node_storage = $entity_type_manager->getStorage('node');
    $this->node_view_builder = $entity_type_manager->getViewBuilder('node');

  }

  /**
   * {@inheritdoc}
   */
  public function getRelatedPostsWithSameExactTags(NodeInterface $node, $limit = 2) {
    $result = &drupal_static(this::class . __METHOD__. $node->id() . $limit);

    if (!isset($result)) {
      if ($node->hasField('field_tags') && !$node->get('field_tags')->isEmpty()) {
        $query = $this->node_storage->getQuery()
          ->condition('status', NodeInterface::PUBLISHED)
          ->condition('type', 'blog_article')
          ->condition('nid', $node->id(), '<>')
          ->range(0, $limit)
          ->addTag('entity_query_random');

        $query->addTag('entity_query_random');

        foreach ($current_tags = $node->get('field_tags')->getValue() as $field_tag) {
          $and = $query->andConditionGroup();
          $and->condition('field_tags', $field_tag['target_id']);
          $query->condition($and);
        }

        $result = $query->execute();

      }
      else {
        $result = [];
      }
    }

    return $result;

  }

  /**
   * {@inheritdoc}
   */
  public function getRelatedPostsWithSameTags(NodeInterface $node, array $exclude_ids = [], $limit = 2) {
    $result = &drupal_static(this::class . __METHOD__ . $node->id() . $limit);

    if (!isset($result)) {
      if ($node->hasField('field_tags') && !$node->get('field_tags')->isEmpty()) {

        $field_tags_ids = [];
        foreach ($current_tags = $node->get('field_tags')->getValue() as $field_tag) {
          $field_tags_ids[] = $field_tag['target_id'];
        }

        $query = $this->node_storage->getQuery()
          ->condition('status', NodeInterface::PUBLISHED)
          ->condition('type', 'blog_article')
          ->condition('nid', $node->id(), '<>')
          ->condition('field_tags', $field_tags_ids, 'IN')
          ->range(0, ($limit))
          ->addTag('entity_query_random');

        if (!empty($exclude_ids)) {
          $query->condition('nid', $exclude_ids, 'NOT IN');
        }
        $result = $query->execute();
      }
      else {
        $result = [];
      }
    }

    return $result;

  }

  /**
   * {@inheritdoc}
   */
  public function getRandomPosts($limit, array $exclude_ids = []) {
    $query = $this->node_storage->getQuery()
      ->condition('status', NodeInterface::PUBLISHED)
      ->condition('type', 'blog_article')
      ->range(0, ($limit));

    if (!empty($exclude_ids)) {
      $query->condition('nid', $exclude_ids, 'NOT IN');
    }

    return $query->execute();

  }

  /**
   * {@inheritdoc}
   */
  public function getRelatedPosts(NodeInterface $node, $max = 4, $exact_tags = 2) {
    $result = &drupal_static(this::class . __METHOD__ . $node->id() . $max  . $exact_tags);

    if (!isset($result)) {
      if ($exact_tags > $max) {
        $exact_tags = $max;
      }

      $exclude_ids = [
        $node->id(),
      ];
      $counter = 0;
      $result = [];
      if ($exact_tags > 0) {
        $exact_same = $this->getRelatedPostsWithSameExactTags($node, $exact_tags);
        $result += $exact_same;
        $counter += count($exact_same);
        $exclude_ids += $exact_same;
      }

      if ($counter < $max) {
        $same_tags = $this->getRelatedPostsWithSameTags($node, $exclude_ids, ($max- $counter));
        $result += $same_tags;
        $counter += count($same_tags);
        $exclude_ids += $same_tags;
      }

      if ($counter < $max) {
        $random = $this->getRandomPosts(($max - $counter), $exclude_ids);
        $result += $random;
      }
    }

    return $result;
  }
}
