<?php

namespace Drupal\dlog_blog\Service;

/**
 * Class for blog lazy builders.
 * @package Drupla\dlog_blDrupal\dlog_blog\Service\og\Service
 */
class BlogLazyBuilder implements BlogLazyBuilderInterface {

  /**
   * {@inheritdoc}
   */
  public static function randomBlogPosts() {
    return [
      '#theme' => 'dlog_blog_random_posts',
    ];
  }
}
