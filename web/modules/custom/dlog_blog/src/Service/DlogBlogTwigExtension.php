<?php

namespace Drupal\dlog_blog\Service;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Twig extension.
 */
class DlogBlogTwigExtension extends \Twig_Extension {
  /**
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config_factory;

  /**
   * DlogBlogTwigExtension constructor.
   * @param ConfigFactoryInterface $config_factory
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config_factory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new \Twig_SimpleFunction('dlog_availability_status', function () {
        $availability_settings = $this->config_factory->get('dlog_blog.availability.settings');

        return $availability_settings->get('status');
      }),
    ];
  }

}
