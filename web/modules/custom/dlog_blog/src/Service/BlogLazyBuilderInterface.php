<?php

namespace Drupal\dlog_blog\Service;

/**
 * Interface BlogLazyBuilderInterface
 * @package Drupal\dlog_blog\Service
 */
interface BlogLazyBuilderInterface {

  /**
   * Gets random posts with theme hook.
   *
   * @return array
   *  Render array with theme hooks dlog_blog_random_posts.
   */
  public static function randomBlogPosts();
}
